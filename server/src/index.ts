import App = require('./app/app');
import config = require('./config');

const app = new App(config);

app.start();
