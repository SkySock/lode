import utils = require('./lib/utils');
import path = require('path');
import { type AppConfig } from './app/types';

const config: AppConfig = {
    port: utils.getEnv('PORT'),
    host: utils.getEnv('HOST', 'localhost'),
    jwt: {
        secret: utils.getEnv('SECRET_JWT'),
        refreshDuration: 3600 * 24 * 60,
        accessDuration: 60 * 5,
    },
    minio: {
        endPoint: utils.getEnv('MINIO_HOST', 'localhost'),
        port: +utils.getEnv('MINIO_PORT', '9000'),
        accessKey: utils.getEnv('AWS_ACCESS_KEY_ID'),
        secretKey: utils.getEnv('AWS_SECRET_ACCESS_KEY'),
        useSSL: false,
    },
    knexConfig: {
        development: {
            client: 'pg',
            connection: {
                host: utils.getEnv('POSTGRES_HOST'),
                port: +utils.getEnv('POSTGRES_PORT', '5432'),
                database: utils.getEnv('POSTGRES_DB'),
                user: utils.getEnv('POSTGRES_USER'),
                password: utils.getEnv('POSTGRES_PASSWORD'),
            },
            pool: {
                min: 2,
                max: 10,
            },
            migrations: {
                directory: path.join(__dirname, './database/migrations'),
            },
            seeds: {
                directory: path.join(__dirname, './database/seeds'),
            },
        },
    },
};

export = config;
