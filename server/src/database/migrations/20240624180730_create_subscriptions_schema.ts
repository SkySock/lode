import type { Knex } from 'knex';

async function up(knex: Knex): Promise<void> {
    return knex.schema
        .createTable(
            'user_subscription_level',
            (table: Knex.CreateTableBuilder) => {
                table.bigIncrements('id');
                table.uuid('owner').notNullable();
                table.string('name', 40).notNullable();
                table.text('description').notNullable().defaultTo('');
                table.string('imageFileName').notNullable().defaultTo('');
                table.decimal('price', 15, 2).notNullable();
                table
                    .timestamp('createdAt', { useTz: false })
                    .notNullable()
                    .defaultTo(knex.fn.now());

                table
                    .foreign('owner')
                    .references('id')
                    .inTable('users')
                    .onDelete('CASCADE');
                // table.index(['owner', 'price'], 'idx_owner_price');
                table.unique(['owner', 'price'], {
                    indexName: 'idx_owner_price',
                    useConstraint: false,
                });
            }
        )
        .createTable('sponsorship_subscriptions', (table) => {
            table.bigIncrements('id');
            table.uuid('user').notNullable();
            table.bigInteger('subscription');
            table
                .timestamp('createdAt', { useTz: false })
                .notNullable()
                .defaultTo(knex.fn.now());

            table.foreign('user').references('users.id').onDelete('CASCADE');
            table
                .foreign('subscription')
                .references('user_subscription_level.id')
                .onDelete('CASCADE');
            table.unique(['user', 'subscription'], {
                indexName: 'unique_user_subscription',
                useConstraint: true,
            });
        });
}

async function down(knex: Knex): Promise<void> {
    return knex.schema
        .dropTable('sponsorship_subscriptions')
        .dropTable('user_subscription_level');
}

export { up, down };
