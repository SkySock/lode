import type { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(
        'follow_relations',
        (table: Knex.CreateTableBuilder) => {
            table
                .uuid('fkUser')
                .notNullable()
                .references('id')
                .inTable('users')
                .onDelete('CASCADE');
            table
                .uuid('fkFollowsUser')
                .notNullable()
                .references('id')
                .inTable('users')
                .onDelete('CASCADE');
            table
                .timestamp('createdAt', { useTz: false })
                .notNullable()
                .defaultTo(knex.fn.now());

            table.primary(['fkUser', 'fkFollowsUser']);
            table.check('?? != ??', ['fkUser', 'fkFollowsUser']);
        }
    );
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('follow_relations');
}
