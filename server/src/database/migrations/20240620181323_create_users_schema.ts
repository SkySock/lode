import type { Knex } from 'knex';

async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable(
        'users',
        (table: Knex.CreateTableBuilder) => {
            table.uuid('id', { primaryKey: true }).defaultTo(knex.fn.uuid());
            table
                .string('username', 40)
                .notNullable()
                .unique({ indexName: 'userUniqueUsername' });
            table.string('displayName', 40).notNullable().defaultTo('');
            table
                .string('email', 40)
                .notNullable()
                .unique({ indexName: 'userUniqueEmail' });
            table.string('password', 255).notNullable();
            table.string('avatar', 255).notNullable().defaultTo('');
            table.text('bio').notNullable().defaultTo('');
            table
                .timestamp('joinedAt', { useTz: false })
                .notNullable()
                .defaultTo(knex.fn.now());
        }
    );
}

async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('users');
}

export = { up, down };
