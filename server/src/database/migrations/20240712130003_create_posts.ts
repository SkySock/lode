import type { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
    return knex.schema
        .createTable('posts', (table: Knex.CreateTableBuilder) => {
            table.uuid('id', { primaryKey: true }).defaultTo(knex.fn.uuid());
            table
                .uuid('fkAuthor')
                .references('id')
                .inTable('users')
                .notNullable();
            table.text('description').notNullable().defaultTo('');
            table
                .enum('status', ['draft', 'waiting', 'published'], {
                    useNative: true,
                    enumName: 'post_status',
                })
                .defaultTo('draft');
            table
                .bigInteger('fkSubscription')
                .references('id')
                .inTable('user_subscription_level')
                .onDelete('SET NULL')
                .defaultTo(null);
            table
                .timestamp('createdAt', { useTz: false })
                .notNullable()
                .defaultTo(knex.fn.now());
            table.timestamp('publishedAt', { useTz: false }).defaultTo(null);
        })

        .createTable('post_media_content', (table: Knex.CreateTableBuilder) => {
            table.bigIncrements('id');
            table
                .uuid('fkPost')
                .references('id')
                .inTable('posts')
                .onDelete('CASCADE')
                .notNullable();
            table.string('fileName').notNullable().defaultTo('');

            table.index('fkPost', 'post_media_content_fkPost_idx');
        })

        .createTable('tags', (table: Knex.CreateTableBuilder) => {
            table.uuid('id', { primaryKey: true }).defaultTo(knex.fn.uuid());
            table.string('name').unique({ indexName: 'tags_name_unique' });
        })

        .createTable('post_tags', (table: Knex.CreateTableBuilder) => {
            table
                .uuid('fkPost')
                .references('id')
                .inTable('posts')
                .onDelete('CASCADE')
                .notNullable();
            table.uuid('fkTag').references('id').inTable('tags').notNullable();
            table.primary(['fkPost', 'fkTag']);
        });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema
        .dropTable('post_tags')
        .dropTable('tags')
        .dropTable('post_media_content')
        .dropTable('posts')
        .raw('DROP TYPE "post_status"');
}
