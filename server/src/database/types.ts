export interface User {
    id: string;
    username: string;
    displayName: string;
    email: string;
    password: string;
    avatar: string;
    bio: string;
    joinedAt: string;
}

export interface SubscriptionLevel {
    id: string;
    owner: User['id'];
    name: string;
    description: string;
    imageFileName: string;
    price: string;
    createdAt: string;
}

export interface SponsorshipSubscription {
    id: string;
    user: User['id'];
    subscription: SubscriptionLevel['id'];
    createdAt: string;
}

export interface FollowRelations {
    fkUser: User['id'];
    fkFollowsUser: User['id'];
    createdAt: string;
}

export interface Post {
    id: string;
    fkAuthor: User['id'];
    description: string;
    status: PostStatus;
    fkSubscription: SubscriptionLevel['id'] | null;
    createdAt: string;
    publishedAt: string | null;
}

export interface PostMediaContent {
    id: string;
    fkPost: Post['id'];
    fileName: string;
}

export interface Tag {
    id: string;
    name: string;
}

export interface PostTag {
    fkPost: Post['id'];
    fkTag: Tag['id'];
}

const postStatuses = ['draft', 'waiting', 'published'] as const;
type PostStatus = (typeof postStatuses)[number];
