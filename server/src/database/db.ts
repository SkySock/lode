import knex = require('knex');
import config = require('../knexfile');
import utils = require('../lib/utils');

const env = utils.getEnv('NODE_ENV', 'development');

const configOptions = config[env];

export = knex(configOptions);
