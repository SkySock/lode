import { type JSONSchemaType } from 'ajv';
import { CreateSubscriptionReqBody } from './types';

import ajv = require('../../lib/ajv');
import type { UploadedFile } from 'express-fileupload';

const createSubscriptionSchema: JSONSchemaType<CreateSubscriptionReqBody> = {
    type: 'object',
    properties: {
        name: { type: 'string' },
        description: { type: 'string' },
        price: { type: 'string' },
    },
    required: ['name', 'price'],
    additionalProperties: false,
};

export = {
    createSubscriptionValidateFn: ajv.compile(createSubscriptionSchema),
};
