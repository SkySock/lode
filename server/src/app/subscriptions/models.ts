import type {
    CreateSubscriptionLevelDTO,
    SubscriptionsModel as SubsModel,
    SubscriptionLevelDTO,
    UpdateSubscriptionLevelDTO,
} from './types';
import db = require('../../database/db');

class SubscriptionsModel implements SubsModel {
    SUBSCRIPTION_LEVEL = 'user_subscription_level';
    SPONSORSHIP_SUBSCRIPTION = 'sponsorship_subscriptions';

    async create(
        data: CreateSubscriptionLevelDTO
    ): Promise<Pick<SubscriptionLevelDTO, 'id'>[]> {
        return db(this.SUBSCRIPTION_LEVEL).insert(data, ['id']);
    }

    async getById(id: number): Promise<SubscriptionLevelDTO | undefined> {
        return db
            .select()
            .from<SubscriptionLevelDTO>(this.SUBSCRIPTION_LEVEL)
            .where('id', id)
            .first();
    }

    async update(
        id: number,
        data: UpdateSubscriptionLevelDTO
    ): Promise<UpdateSubscriptionLevelDTO[]> {
        return db(this.SUBSCRIPTION_LEVEL)
            .where('id', id)
            .update(data, [...Object.keys(data)]);
    }
}

export = new SubscriptionsModel();
