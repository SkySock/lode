import type {
    SubscriptionsService,
    SubscriptionsController as SubsController,
    CreateSubscriptionServiceLayerDTO,
} from './types';
import type { NextFunction, Request, Response } from 'express';
import type { AuthorizedRequest } from '../types';

import validators = require('./validators');
import libValidators = require('../../lib/validators');
import errors = require('../../lib/errors');
import utils = require('../../lib/utils');

class SubscriptionsController implements SubsController {
    #subscriptionsService: SubscriptionsService;

    constructor(subscriptionsService: SubscriptionsService) {
        this.#subscriptionsService = subscriptionsService;
    }

    createSubscription = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        if (!validators.createSubscriptionValidateFn(req.body)) {
            return next(
                new errors.HttpError(
                    400,
                    'BODY_VALIDATION',
                    utils.ajvErrorsToResponseMessage(
                        validators.createSubscriptionValidateFn.errors
                    )
                )
            );
        }

        const image = req.files?.image;
        if (!image || Array.isArray(image)) {
            return next(new errors.HttpError(400));
        }

        if (
            !libValidators.validateImageFile(
                image,
                ['png', 'jpg', 'jpeg'],
                2 * 1024 * 1024
            )
        ) {
            return next(
                new errors.HttpError(
                    400,
                    'FILE_VALIDATION',
                    'Файл изображения должен иметь расширение png, jpg или jpeg и размер не более 2 Мб'
                )
            );
        }

        const newSubscription: CreateSubscriptionServiceLayerDTO = {
            owner: idUser,
            ...req.body,
        };

        try {
            const subscription =
                await this.#subscriptionsService.createSubscription(
                    newSubscription,
                    image
                );
            return res.status(201).json(subscription);
        } catch (error) {
            if (error instanceof errors.ServiceError) {
                switch (error.code) {
                    case 'SUBSCRIPTION_PRICE_EXISTS':
                        return next(
                            new errors.HttpError(
                                400,
                                'SUBSCRIPTION_PRICE_EXISTS',
                                'У пользователя уже существует подписка с такой стоимостью'
                            )
                        );
                    // break;
                    default:
                        break;
                }
            }

            return next(new errors.HttpError(500, 'GENERIC'));
        }
    };

    getSubscription = async (req: Request, res: Response) => {
        const id = +req.params.id;
        if (!Number.isInteger(id)) {
            return res.sendStatus(400);
        }

        try {
            const subscription =
                await this.#subscriptionsService.getSubscription(id);
            return res.status(200).json(subscription);
        } catch (error) {
            return res.sendStatus(404);
        }
    };

    updateSubscription = async (req: Request, res: Response) => {};
}

export = SubscriptionsController;
