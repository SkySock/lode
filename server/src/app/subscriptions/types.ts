import type { Request, Response, NextFunction } from 'express';
import type { UploadedFile } from 'express-fileupload';
import type { SubscriptionLevel } from '../../database/types';

export interface SubscriptionsModel {
    create(
        data: CreateSubscriptionLevelDTO
    ): Promise<Pick<SubscriptionLevelDTO, 'id'>[]>;
    getById(id: number): Promise<SubscriptionLevelDTO | undefined>;
    update(
        id: number,
        data: UpdateSubscriptionLevelDTO
    ): Promise<UpdateSubscriptionLevelDTO[]>;
}

export interface SubscriptionsService {
    createSubscription(
        data: CreateSubscriptionServiceLayerDTO,
        image: UploadedFile
    ): Promise<Pick<SubscriptionLevelDTO, 'id'>>;
    getSubscription(id: number): Promise<RetrieveSubscriptionLevelDTO>;
    updateSubscription(
        id: number,
        data: UpdateSubscriptionLevelDTO
    ): Promise<UpdateSubscriptionLevelDTO[]>;
}

export interface SubscriptionsController {
    createSubscription: (
        req: Request,
        res: Response,
        next: NextFunction
    ) => void;
    getSubscription: (req: Request, res: Response, next: NextFunction) => void;
}

export type SubscriptionLevelDTO = SubscriptionLevel;

export type CreateSubscriptionLevelDTO = Omit<
    SubscriptionLevelDTO,
    'id' | 'createdAt'
>;
export type UpdateSubscriptionLevelDTO = Partial<
    Omit<SubscriptionLevelDTO, 'owner' | 'price | id'>
>;
export type CreateSubscriptionReqBody = Pick<
    SubscriptionLevelDTO,
    'name' | 'description' | 'price'
>;

export type CreateSubscriptionServiceLayerDTO = Omit<
    SubscriptionLevelDTO,
    'id' | 'imageFileName' | 'createdAt'
>;

export type RetrieveSubscriptionLevelDTO = {
    id: string;
    owner: string;
    name: string;
    description: string;
    imageUrl: string;
    price: string;
    createdAt: string;
};
