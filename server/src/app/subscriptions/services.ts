import type {
    CreateSubscriptionLevelDTO,
    CreateSubscriptionServiceLayerDTO,
    RetrieveSubscriptionLevelDTO,
    SubscriptionsService as SubsService,
    SubscriptionsModel,
    UpdateSubscriptionLevelDTO,
} from './types';
import type { Client } from 'minio';
import type { UploadedFile } from 'express-fileupload';
import errors = require('../../lib/errors');
import uuid = require('uuid');
import s3 = require('../../lib/s3');
import utils = require('../../lib/utils');

class SubscriptionsService implements SubsService {
    #subscriptionsModel: SubscriptionsModel;
    #minioClient: Client;

    constructor(subscriptionsModel: SubscriptionsModel, minioClient: Client) {
        this.#subscriptionsModel = subscriptionsModel;
        this.#minioClient = minioClient;
    }

    async createSubscription(
        data: CreateSubscriptionServiceLayerDTO,
        image: UploadedFile
    ) {
        const s3Params = {
            bucket: 'subscription-level-images',
            destinationObj: `${uuid.v4()}.${utils.getFileExtension(image)}`,
            file: image.data,
        };

        const subscriptionData = {
            ...data,
            imageFileName: s3Params.destinationObj,
        };

        let subs;
        try {
            subs = (await this.#subscriptionsModel.create(subscriptionData))[0];
        } catch (error) {
            if (error instanceof Error) {
                if (
                    error.message.includes(
                        'unique constraint "idx_owner_price"'
                    )
                ) {
                    throw new errors.ServiceError('SUBSCRIPTION_PRICE_EXISTS');
                }
            }

            throw error;
        }

        try {
            await s3.createBucket(this.#minioClient, s3Params.bucket);

            await this.#minioClient.putObject(
                s3Params.bucket,
                s3Params.destinationObj,
                s3Params.file
            );
        } catch (error) {
            console.log(error);
        }

        return subs;
    }

    async getSubscription(id: number): Promise<RetrieveSubscriptionLevelDTO> {
        const subscription = await this.#subscriptionsModel.getById(id);

        if (!subscription) {
            throw new errors.ServiceError('SUBSCRIPTIONS_LEVEL_NOT_FOUND');
        }

        let subscriptionResult: RetrieveSubscriptionLevelDTO = {
            id: subscription.id,
            owner: subscription.owner,
            name: subscription.name,
            description: subscription.description,
            imageUrl: '',
            price: subscription.price,
            createdAt: subscription.createdAt,
        };

        if (subscription.imageFileName.length === 0) {
            return subscriptionResult;
        }

        try {
            const imageUrl = await this.#minioClient.presignedUrl(
                'GET',
                'subscription-level-images',
                subscription.imageFileName,
                3600 * 24 * 7
            );
            subscriptionResult.imageUrl = imageUrl;
        } catch (error) {
            console.log(error);
        }

        return subscriptionResult;
    }

    async updateSubscription(id: number, data: UpdateSubscriptionLevelDTO) {
        throw Error('Not implemented');
        return this.#subscriptionsModel.update(id, data);
    }
}

export = SubscriptionsService;
