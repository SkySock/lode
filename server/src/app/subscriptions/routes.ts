import express = require('express');
import type { SubscriptionsController } from './types';
import type { AuthMiddleware } from '../types';

const subscriptionsRouter = (
    controller: SubscriptionsController,
    authMiddleware: AuthMiddleware
) => {
    const router = express.Router();

    router.post('/', authMiddleware, controller.createSubscription);
    router.get('/:id', controller.getSubscription);

    return router;
};

export = subscriptionsRouter;
