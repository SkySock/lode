import jwt = require('jsonwebtoken');
import uuid = require('uuid');
import type { UploadedFile } from 'express-fileupload';
import type { Client } from 'minio';
import type {
    CreateUserDTO,
    RegistrationUserDTO,
    UserModel,
    AuthService as IAuthService,
    AuthCredentialsDTO,
    RefreshJWTPayload,
    AccessJWTPayload,
    RetrieveUser,
    MinimalUserInfoDTO,
} from './types';
import type { JWTOptions } from '../types';

import crypto = require('../../lib/crypto');
import errors = require('../../lib/errors');
import utils = require('../../lib/utils');
import s3 = require('../../lib/s3');

class AuthService implements IAuthService {
    readonly S3_AVATAR_BUCKET = 'user-avatars';

    #userModel: UserModel;
    #jwtOptions: JWTOptions;
    #minioClient: Client;

    constructor(
        userModel: UserModel,
        jwtOptions: JWTOptions,
        minioClient: Client
    ) {
        this.#userModel = userModel;
        this.#jwtOptions = jwtOptions;
        this.#minioClient = minioClient;
    }

    async register(data: RegistrationUserDTO, avatarFile?: UploadedFile) {
        let avatarFileName = '';

        if (avatarFile) {
            avatarFileName = `${uuid.v4()}.${utils.getFileExtension(
                avatarFile
            )}`;
        }

        let user: MinimalUserInfoDTO | undefined;

        try {
            const newUser: CreateUserDTO = {
                ...data,
                password: await crypto.hashPassword(data.password),
                avatar: avatarFileName,
            };

            user = (await this.#userModel.create(newUser))[0];
        } catch (e) {
            throw new errors.ServiceError('USER_EXISTS');
        }

        try {
            if (avatarFile) {
                await s3.createBucket(this.#minioClient, this.S3_AVATAR_BUCKET);

                this.#minioClient.putObject(
                    this.S3_AVATAR_BUCKET,
                    avatarFileName,
                    avatarFile.data
                );
            }
        } catch (e) {
            console.log(e);
        }

        return user;
    }

    async newAuthTokens(credentials: AuthCredentialsDTO) {
        try {
            const user = await this.#userModel.getPassword(
                credentials.username
            );

            if (!user) {
                throw new errors.ServiceError('USER_NOT_FOUND');
            }

            const isVerified = await crypto.verifyPassword(
                credentials.password,
                user.password
            );

            if (!isVerified) {
                throw new errors.ServiceError('WRONG_PASSWORD');
            }

            const refreshPayload: RefreshJWTPayload = {
                sub: user.id,
            };
            const accessPayload: AccessJWTPayload = {
                sub: user.id,
            };

            return {
                refresh: this.#generateJWT(refreshPayload, 'refresh'),
                access: this.#generateJWT(accessPayload, 'access'),
            };
        } catch (error) {
            if (error instanceof errors.ServiceError) {
                throw error;
            }

            throw new errors.ServiceError();
        }
    }

    async getUserInfo(idUser: string) {
        const user = await this.#userModel.getUserInfo(idUser);

        if (!user) {
            throw new errors.ServiceError('USER_NOT_FOUND');
        }

        let userResultInfo: RetrieveUser = {
            id: user.id,
            username: user.username,
            displayName: user.displayName,
            avatarUrl: '',
            bio: user.bio,
            joinedAt: user.joinedAt,
        };

        if (user.avatar.length === 0) {
            return userResultInfo;
        }

        try {
            const avatarUrl = await this.#minioClient.presignedUrl(
                'GET',
                this.S3_AVATAR_BUCKET,
                user.avatar,
                3600 * 24
            );
            userResultInfo.avatarUrl = avatarUrl;
        } catch (error) {
            console.log(error);
        }

        return userResultInfo;
    }

    async getAccessToken(refreshToken: string) {
        try {
            const payload = jwt.verify(
                refreshToken,
                this.#jwtOptions.secret
            ) as RefreshJWTPayload;

            const accessPayload: AccessJWTPayload = {
                sub: payload.sub,
            };

            return this.#generateJWT(accessPayload, 'access');
        } catch (e) {
            throw new errors.ServiceError('WRONG_REFRESH_TOKEN');
        }
    }

    verifyAccessToken(token: string) {
        return jwt.verify(token, this.#jwtOptions.secret) as AccessJWTPayload;
    }

    #generateJWT<P extends AccessJWTPayload | RefreshJWTPayload>(
        payload: P,
        typeToken: 'access' | 'refresh'
    ): string {
        const now = Math.floor(Date.now() / 1000);

        payload.iat = now;
        payload.exp =
            now +
            (typeToken === 'refresh'
                ? this.#jwtOptions.refreshDuration
                : this.#jwtOptions.accessDuration);

        return jwt.sign(payload, this.#jwtOptions.secret);
    }
}

export = { AuthService };
