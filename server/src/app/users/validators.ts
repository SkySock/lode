import type { AuthCredentialsDTO, RegistrationUserDTO } from './types';
import { type JSONSchemaType } from 'ajv';

import ajv = require('../../lib/ajv');

const authSchema: JSONSchemaType<AuthCredentialsDTO> = {
    type: 'object',
    properties: {
        password: { type: 'string' },
        username: { type: 'string' },
    },
    required: ['password', 'username'],
    additionalProperties: false,
};

const registrationSchema: JSONSchemaType<RegistrationUserDTO> = {
    type: 'object',
    properties: {
        email: { type: 'string', format: 'email' },
        password: { type: 'string' },
        username: { type: 'string' },
        displayName: { type: 'string', nullable: true },
    },
    required: ['email', 'password', 'username'],
    additionalProperties: false,
};

export = {
    authValidateFn: ajv.compile(authSchema),
    registrationValidateFn: ajv.compile(registrationSchema),
};
