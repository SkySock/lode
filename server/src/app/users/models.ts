import type {
    CreateUserDTO,
    UserModel as IUserModel,
    MinimalUserInfoDTO,
    UserDTO,
    UserInfo,
} from './types';
import db = require('../../database/db');

class UserModel implements IUserModel {
    USERS = 'users';

    async create(userData: CreateUserDTO): Promise<MinimalUserInfoDTO[]> {
        return db('users').insert(userData, ['id', 'username', 'email']);
    }

    async getPassword(username: string) {
        return db
            .select('id', 'password')
            .from<UserDTO>('users')
            .where('username', username)
            .first();
    }

    async getUserInfo(idUser: string) {
        return db
            .select<UserInfo>(
                'id',
                'username',
                'displayName',
                'email',
                'avatar',
                'bio',
                'joinedAt'
            )
            .from<UserDTO>('users')
            .where('id', idUser)
            .first();
    }
}

export = new UserModel();
