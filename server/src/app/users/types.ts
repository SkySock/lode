import type e = require('express');
import type { JwtPayload } from 'jsonwebtoken';
import type { UploadedFile } from 'express-fileupload';
import type { User } from '../../database/types';

export interface UserModel {
    create(data: CreateUserDTO): Promise<MinimalUserInfoDTO[]>;
    getPassword(
        username: string
    ): Promise<Pick<UserDTO, 'id' | 'password'> | undefined>;
    getUserInfo(username: string): Promise<UserInfo | undefined>;
}

export interface AuthController {
    register(req: e.Request, res: e.Response, next: e.NextFunction): void;
    getAccessToken(req: e.Request, res: e.Response, next: e.NextFunction): void;
    getRefreshToken(
        req: e.Request,
        res: e.Response,
        next: e.NextFunction
    ): void;
    getViewer(req: e.Request, res: e.Response, next: e.NextFunction): void;
}

export interface AuthService {
    register(
        data: RegistrationUserDTO,
        avatar?: UploadedFile
    ): Promise<MinimalUserInfoDTO>;
    newAuthTokens(data: AuthCredentialsDTO): Promise<AuthTokens>;
    getAccessToken(refreshToken: string): Promise<string>;
    verifyAccessToken(token: string): AccessJWTPayload;
    getUserInfo(idUser: string): Promise<RetrieveUser>;
}

export interface AuthCredentialsDTO {
    username: string;
    password: string;
}

export interface RegistrationUserDTO {
    username: string;
    email: string;
    password: string;
    displayName?: string;
}

export type UserDTO = User;

export type CreateUserDTO = Pick<
    UserDTO,
    'username' | 'email' | 'password' | 'avatar'
>;
export type MinimalUserInfoDTO = Pick<UserDTO, 'id' | 'username' | 'email'>;
export type UserInfo = Omit<UserDTO, 'password'>;
export type RetrieveUser = {
    id: string;
    username: string;
    displayName: string;
    avatarUrl: string;
    bio: string;
    joinedAt: string;
};

export interface AccessJWTPayload extends JwtPayload {
    sub: string;
}

export interface RefreshJWTPayload extends JwtPayload {
    sub: string;
}

type AuthTokens = {
    refresh: string;
    access: string;
};
