import type { AuthController as IAuthController } from './types';
import type { NextFunction, Request, Response } from 'express';
import type { AuthService } from './types';
import validators = require('./validators');
import libValidators = require('../../lib/validators');
import errors = require('../../lib/errors');
import utils from '../../lib/utils';
import { AuthorizedRequest } from '../types';

class AuthController implements IAuthController {
    #authService: AuthService;

    constructor(authService: AuthService) {
        this.#authService = authService;
    }

    register = async (req: Request, res: Response, next: NextFunction) => {
        if (!validators.registrationValidateFn(req.body)) {
            return next(
                new errors.HttpError(
                    400,
                    'BODY_VALIDATION',
                    utils.ajvErrorsToResponseMessage(
                        validators.registrationValidateFn.errors
                    )
                )
            );
        }

        const avatarImage = req.files?.avatar;
        if (Array.isArray(avatarImage)) {
            return next(
                new errors.HttpError(
                    400,
                    'FILE_VALIDATION',
                    'Avatar image should not be an array'
                )
            );
        }

        if (
            avatarImage &&
            !libValidators.validateImageFile(
                avatarImage,
                ['png', 'jpg', 'jpeg'],
                2 * 1024 * 1024
            )
        ) {
            return next(
                new errors.HttpError(
                    400,
                    'FILE_VALIDATION',
                    'Файл изображения должен иметь расширение png, jpg или jpeg и размер не более 2 Мб'
                )
            );
        }

        try {
            const user = await this.#authService.register(
                req.body,
                avatarImage
            );

            return res.status(201).json(user);
        } catch (error) {
            return next(
                new errors.HttpError(
                    400,
                    'USER_EXISTS',
                    'Username or email already exists'
                )
            );
        }
    };

    getAccessToken = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const refresh = req.cookies.refresh;

        if (!refresh) {
            return next(
                new errors.HttpError(
                    401,
                    'WRONG_CREDENTIALS',
                    'Wrong credentials'
                )
            );
        }

        try {
            const access = await this.#authService.getAccessToken(refresh);
            return res.status(201).json({ access });
        } catch (error) {
            return next(new errors.HttpError(403));
        }
    };

    getRefreshToken = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        if (!validators.authValidateFn(req.body)) {
            return next(
                new errors.HttpError(
                    400,
                    'BODY_VALIDATION',
                    utils.ajvErrorsToResponseMessage(
                        validators.authValidateFn.errors
                    )
                )
            );
        }

        try {
            const tokens = await this.#authService.newAuthTokens(req.body);
            res.cookie('refresh', tokens.refresh, { httpOnly: true });

            return res.status(201).json({ access: tokens.access });
        } catch (error) {
            return next(
                new errors.HttpError(
                    401,
                    'WRONG_CREDENTIALS',
                    'Wrong username or password'
                )
            );
        }
    };

    getViewer = async (req: Request, res: Response, next: NextFunction) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        try {
            const user = await this.#authService.getUserInfo(idUser);

            if (!user) {
                return next(
                    new errors.HttpError(
                        404,
                        'USER_NOT_FOUND',
                        'User not found'
                    )
                );
            }

            return res.status(200).json(user);
        } catch (error) {
            return next(new errors.HttpError(500));
        }
    };
}

export = { AuthController };
