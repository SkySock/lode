import express = require('express');
import type { AuthController } from './types';
import type { AuthMiddleware } from '../types';

const authRouter = (
    controller: AuthController,
    authMiddleware: AuthMiddleware
) => {
    const router = express.Router();

    router.post('/register', controller.register);
    router.post('/refresh', controller.getRefreshToken);
    router.get('/access', controller.getAccessToken);
    router.get('/me', authMiddleware, controller.getViewer);

    return router;
};

export = { authRouter };
