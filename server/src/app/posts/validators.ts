import { type JSONSchemaType } from 'ajv';

import ajv = require('../../lib/ajv');
import type { CreatePostReqBody } from './types';

const createPostSchema: JSONSchemaType<CreatePostReqBody> = {
    type: 'object',
    properties: {
        description: { type: 'string', default: '' },
        fkSubscription: { type: 'string' },
        'tags[]': { type: 'array', items: { type: 'string' }, default: [] },
    },

    required: ['description'],
    additionalProperties: false,
};

export = {
    createPostValidateFn: ajv.compile(createPostSchema),
};
