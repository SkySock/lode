import type { Client } from 'minio';
import type { Post } from '../../database/types';
import type {
    CreatePostModelDTO,
    CreatePostReqBody,
    PostsModel,
    PostsService as PService,
    RetrieveMediaContents,
    RetrievePostResponse,
} from './types';
import type { UploadedFile } from 'express-fileupload';
import s3 = require('../../lib/s3');
import crypto = require('crypto');

class PostService implements PService {
    readonly BUCKET = 'posts';
    #postsModel;
    #minioClient: Client;

    constructor(postsModel: PostsModel, minioClient: Client) {
        this.#postsModel = postsModel;
        this.#minioClient = minioClient;
    }

    async createPost(
        postData: CreatePostReqBody,
        authorId: string,
        files: UploadedFile[]
    ): Promise<Pick<Post, 'id'> | undefined> {
        const postId = crypto.randomUUID();

        let postCreateData: CreatePostModelDTO = {
            id: postId,
            description: postData.description,
            fkAuthor: authorId,
            fkSubscription: postData.fkSubscription,
            status: 'published',
            publishedAt: null,
            tags: postData['tags[]'],
            mediaContents: files.map((f) => ({
                fileName: `${postId}/${f.name}`,
            })),
        };

        console.log(postCreateData);

        let post: Pick<Post, 'id'> | undefined;

        try {
            post = await this.#postsModel.createPost(postCreateData);
        } catch (error) {
            console.log(error);
        }

        try {
            await s3.createBucket(this.#minioClient, this.BUCKET);

            const objectsInfo = await Promise.all(
                files.map((f) =>
                    this.#minioClient.putObject(
                        this.BUCKET,
                        `${postId}/${f.name}`,
                        f.data
                    )
                )
            );
            console.log(objectsInfo);
        } catch (error) {
            console.log(error);
        }

        return post;
    }

    async retrievePost(postId: string, viewerId?: string) {
        const rawPost = await this.#postsModel.getPostInfo(postId);

        if (!rawPost) {
            return;
        }

        const mediaContents: RetrieveMediaContents[] = await Promise.all(
            rawPost.mediaContents.map(async (c) => {
                return {
                    id: c.id,
                    fileSrc: await this.#minioClient.presignedUrl(
                        'GET',
                        this.BUCKET,
                        c.fileName,
                        3600 * 3
                    ),
                };
            })
        );

        const postInfo: RetrievePostResponse = {
            ...rawPost,
            mediaContents: mediaContents,
        };

        return postInfo;
    }

    async deletePost(postId: string, viewerId: string) {
        const post = await this.#postsModel.getPostInfo(postId);

        if (!post) {
            throw new Error('PostNotFound');
        }

        if (post.fkAuthor !== viewerId) {
            throw new Error('Access Denied');
        }

        const deletedPost = await this.#postsModel.deletePost(postId);
        if (deletedPost.length !== 1) {
            throw new Error();
        }
    }

    async getPublicUserPosts(userId: string, page: number, count: number) {
        const rawPosts = await this.#postsModel.getUserPosts(
            userId,
            page,
            count,
            'published'
        );

        const posts = await Promise.all(
            rawPosts.map(async (post) => {
                const mediaContents: RetrieveMediaContents[] =
                    await Promise.all(
                        post.mediaContents.map(async (c) => {
                            return {
                                id: c.id,
                                fileSrc: await this.#minioClient.presignedUrl(
                                    'GET',
                                    this.BUCKET,
                                    c.fileName,
                                    3600 * 3
                                ),
                            };
                        })
                    );

                const postInfo: RetrievePostResponse = {
                    ...post,
                    mediaContents,
                };

                return postInfo;
            })
        );

        return {
            posts,
            page: page,
        };
    }
}

export = PostService;
