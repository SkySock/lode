import db from '../../database/db';
import type {
    Post,
    PostMediaContent,
    PostTag,
    Tag,
} from '../../database/types';
import type {
    CreatePostModelDTO,
    PostsModel as PModel,
    RawPostFullInfo,
    RetrieveRawMediaContents,
} from './types';

class PostsModel implements PModel {
    readonly T_POSTS = 'posts';
    readonly T_POST_MEDIA_CONTENT = 'post_media_content';
    readonly T_TAGS = 'tags';
    readonly T_POST_TAGS = 'post_tags';

    async createPost(postData: CreatePostModelDTO) {
        const trx = await db.transaction();

        const publishedAt =
            postData.status === 'published' ? db.fn.now() : null;

        try {
            const post = (
                await trx<Post>(this.T_POSTS).insert(
                    {
                        id: postData.id,
                        description: postData.description,
                        fkAuthor: postData.fkAuthor,
                        status: postData.status,
                        fkSubscription: postData.fkSubscription,
                        publishedAt: publishedAt,
                    },
                    ['id']
                )
            )[0];

            if (postData.tags.length != 0) {
                const tags = await trx<Tag>(this.T_TAGS)
                    .insert(
                        postData.tags.map((tag) => ({ name: tag })),
                        ['id']
                    )
                    .onConflict('name')
                    .merge();

                await trx<PostTag>(this.T_POST_TAGS).insert(
                    tags.map((tag) => ({ fkPost: post.id, fkTag: tag.id }))
                );
            }

            const postMediaContents = postData.mediaContents.map(
                (content): Omit<PostMediaContent, 'id'> => ({
                    fkPost: post.id,
                    fileName: content.fileName,
                })
            );

            await trx<PostMediaContent>(this.T_POST_MEDIA_CONTENT).insert(
                postMediaContents,
                ['id']
            );

            await trx.commit();

            return post;
        } catch (error) {
            console.log(error);

            await trx.rollback();
        }
    }

    async getPostInfo(postId: string) {
        const post = await db<Post>(this.T_POSTS)
            .select(
                'id',
                'description',
                'fkAuthor',
                'fkSubscription',
                'publishedAt',
                'status'
            )
            .where('id', postId)
            .first();

        if (!post) {
            return;
        }

        const postInfo: RawPostFullInfo = {
            ...post,
            tags: await this.getPostTags(postId),
            mediaContents: await this.getPostMediaContents(postId),
        };

        return postInfo;
    }

    async getUserPosts(
        userId: string,
        page: number = 1,
        count: number = 10,
        status?: Post['status']
    ) {
        const postsQuery = db<Post>(this.T_POSTS)
            .select(
                'id',
                'fkAuthor',
                'fkSubscription',
                'status',
                'publishedAt',
                'description'
            )
            .where('fkAuthor', userId)
            .orderBy('publishedAt', 'desc');

        if (status) {
            postsQuery.andWhere('status', status);
        }

        postsQuery.limit(count).offset((page - 1) * count);

        const posts = await postsQuery;
        const postsId = posts.map((p) => p.id);

        const postMediaContent = await db<PostMediaContent>(
            this.T_POST_MEDIA_CONTENT
        )
            .select('id', 'fkPost', 'fileName')
            .whereIn('fkPost', postsId);

        const tags: {
            postId: Post['id'];
            tagId: Tag['id'];
            name: Tag['name'];
        }[] = await db<PostTag>({
            pt: this.T_POST_TAGS,
        })
            .select({
                postId: 'pt.fkPost',
                tagId: 't.id',
                name: 't.name',
            })
            .join({ t: this.T_TAGS }, 'pt.fkTag', 't.id')
            .whereIn('pt.fkPost', postsId);

        return posts.map((post) => {
            const pTags: Pick<Tag, 'id' | 'name'>[] = tags
                .filter((t) => t.postId === post.id)
                .map((t) => ({ id: t.tagId, name: t.name }));
            const mediaContents: RetrieveRawMediaContents[] = postMediaContent
                .filter((mediaContent) => mediaContent.fkPost === post.id)
                .map((mediaContent) => ({
                    id: mediaContent.id,
                    fileName: mediaContent.fileName,
                }));

            return {
                ...post,
                tags: pTags,
                mediaContents,
            };
        });
    }

    async toPublishPost(postId: string) {
        return db<Post>(this.T_POSTS).where('id', postId).update(
            {
                publishedAt: db.fn.now(),
                status: 'published',
            },
            ['id', 'publishedAt', 'status']
        );
    }

    deletePost(postId: string) {
        return db<Post>(this.T_POSTS).where('id', postId).delete('id');
    }

    getPostTags(postId: string) {
        return db({
            pt: this.T_POST_TAGS,
        })
            .select('t.id', 't.name')
            .join({ t: this.T_TAGS }, 'pt.fkTag', 't.id')
            .where('pt.fkPost', postId);
    }

    getPostMediaContents(postId: string) {
        return db<PostMediaContent>(this.T_POST_MEDIA_CONTENT)
            .select('id', 'fileName')
            .where('fkPost', postId);
    }
}

export = new PostsModel();
