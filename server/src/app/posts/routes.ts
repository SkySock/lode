import express = require('express');
import type { PostsController } from './types';
import type { AuthMiddleware } from '../types';

const postsRouter = (
    controller: PostsController,
    authMiddleware: AuthMiddleware
) => {
    const router = express.Router();

    router.post('/', authMiddleware, controller.createPost);
    router.get('/', controller.getUserPosts);
    router.get('/:postId', controller.retrievePost);
    router.delete('/:postId', authMiddleware, controller.deletePost)

    return router;
};

export = postsRouter;
