import type { UploadedFile } from 'express-fileupload';
import type { Post, PostMediaContent, Tag } from '../../database/types';
import type { Request, Response, NextFunction } from 'express';

export interface PostsModel {
    createPost(
        postData: CreatePostModelDTO
    ): Promise<Pick<Post, 'id'> | undefined>;
    getPostInfo(postId: string): Promise<RawPostFullInfo | undefined>;
    getUserPosts(
        userId: string,
        page: number,
        count: number,
        status?: Post['status']
    ): Promise<RawPostFullInfo[]>;
    toPublishPost(
        postId: string
    ): Promise<Pick<Post, 'id' | 'publishedAt' | 'status'>[]>;
    deletePost(postId: string): Promise<Pick<Post, 'id'>[]>;
}

export interface PostsService {
    createPost(
        postData: CreatePostReqBody,
        authorId: string,
        files: UploadedFile[]
    ): Promise<Pick<Post, 'id'> | undefined>;
    retrievePost(
        postId: string,
        viewerId?: string
    ): Promise<RetrievePostResponse | undefined>;
    getPublicUserPosts(
        userId: string,
        page: number,
        count: number
    ): Promise<ListPostsResponse>;
    deletePost(postId: string, viewerId: string): Promise<void>;
}

export interface PostsController {
    createPost: Controller;
    retrievePost: Controller;
    getUserPosts: Controller;
    deletePost: Controller;
}

export interface CreatePostModelDTO extends Omit<Post, 'createdAt'> {
    tags: string[];
    mediaContents: CreatePostMediaContent[];
}

export interface CreatePostReqBody
    extends Pick<Post, 'description' | 'fkSubscription'> {
    'tags[]': string[];
}

export type CreatePostMediaContent = Omit<PostMediaContent, 'id' | 'fkPost'>;

export interface RawPostFullInfo {
    id: string;
    fkAuthor: string;
    description: string;
    fkSubscription: string | null;
    status: string;
    tags: Pick<Tag, 'id' | 'name'>[];
    mediaContents: RetrieveRawMediaContents[];
    publishedAt: string | null;
}

export interface RetrievePostResponse
    extends Omit<RawPostFullInfo, 'mediaContents'> {
    mediaContents: RetrieveMediaContents[];
}

export type RetrieveRawMediaContents = Pick<
    PostMediaContent,
    'id' | 'fileName'
>;
export interface RetrieveMediaContents {
    id: string;
    fileSrc: string;
}

export interface ListPostsResponse {
    posts: RetrievePostResponse[];
    page: number;
}

type Controller = (req: Request, res: Response, next: NextFunction) => void;
