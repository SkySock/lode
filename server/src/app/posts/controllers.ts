import type {
    PostsService,
    PostsController as PController,
    CreatePostModelDTO,
} from './types';
import type { Request, Response, NextFunction } from 'express';
import type { AuthorizedRequest } from '../types';
import validators = require('./validators');
import errors = require('../../lib/errors');
import utils = require('../../lib/utils');
import libValidators = require('../../lib/validators');

class PostsController implements PController {
    #postsService: PostsService;

    constructor(postsService: PostsService) {
        this.#postsService = postsService;
    }

    createPost = async (req: Request, res: Response, next: NextFunction) => {
        const authorId = (req as AuthorizedRequest).userId;
        if (!authorId) {
            throw new Error('Must be authenticated');
        }

        if (!validators.createPostValidateFn(req.body)) {
            return next(
                new errors.HttpError(
                    400,
                    'BODY_VALIDATION',
                    utils.ajvErrorsToResponseMessage(
                        validators.createPostValidateFn.errors
                    )
                )
            );
        }

        let files = req.files?.['mediaFiles[]'] || [];

        files = Array.isArray(files) ? files : [files];

        if (files.length < 1) {
            return next(
                new errors.HttpError(
                    400,
                    'BODY_VALIDATION',
                    'Length of field "mediaFiles" must be >= 1'
                )
            );
        }

        if (
            !files
                .map((f) =>
                    libValidators.validateImageFile(
                        f,
                        ['jpeg', 'png', 'jpg'],
                        4 * 1024 * 1024
                    )
                )
                .reduce((acc, v) => acc && v)
        ) {
            return next(new errors.HttpError(400, 'FILE_VALIDATION'));
        }

        try {
            const post = await this.#postsService.createPost(
                req.body,
                authorId,
                files
            );

            res.status(201).json(post);
        } catch (error) {
            console.log(error);

            return next(new errors.HttpError());
        }
    };

    retrievePost = async (req: Request, res: Response, next: NextFunction) => {
        const postId = req.params.postId;
        try {
            const postInfo = await this.#postsService.retrievePost(postId);

            if (!postInfo) {
                return next(
                    new errors.HttpError(
                        404,
                        'NOT_FOUND',
                        `Post with id ${postId} not found`
                    )
                );
            }

            res.status(200).json(postInfo);
        } catch (error) {
            console.log(error);
            return next(new errors.HttpError(500));
        }
    };

    deletePost = async (req: Request, res: Response, next: NextFunction) => {
        const viewerId = (req as AuthorizedRequest).userId;
        if (!viewerId) {
            throw new Error('Must be authenticated');
        }

        const postId = req.params.postId;

        try {
            await this.#postsService.deletePost(postId, viewerId);
            return res.status(204);
        } catch (error) {
            next(new errors.HttpError(400));
        }
    };

    getUserPosts = async (req: Request, res: Response, next: NextFunction) => {
        const { authorId, page, count } = req.query;
        // TODO:
        if (
            typeof authorId !== 'string' ||
            typeof page !== 'string' ||
            typeof count !== 'string'
        ) {
            return next(new errors.HttpError(400));
        }

        try {
            const posts = await this.#postsService.getPublicUserPosts(
                authorId,
                +page,
                +count
            );

            res.status(200).json(posts);
        } catch (error) {
            console.log(error);

            return next(new errors.HttpError(500));
        }
    };
}

export = PostsController;
