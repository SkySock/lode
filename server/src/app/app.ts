import express = require('express');
import http = require('http');
import cors = require('cors');
import morgan = require('morgan');
import cookieParser = require('cookie-parser');
import minio = require('minio');
import fileUpload = require('express-fileupload');

import userModel = require('./users/models');
import subscriptionsModel = require('./subscriptions/models');
import profileModel = require('./profiles/models');
import postsModel = require('./posts/models');

import userServices = require('./users/services');
import SubscriptionsService = require('./subscriptions/services');
import ProfileService = require('./profiles/services');
import PostsService = require('./posts/services');

import userControllers = require('./users/controllers');
import SubscriptionsController = require('./subscriptions/controllers');
import ProfileController = require('./profiles/controllers');
import PostsController = require('./posts/controllers');

import userRoutes = require('./users/routes');
import subsRoutes = require('./subscriptions/routes');
import profileRoutes = require('./profiles/routes');
import postsRoutes = require('./posts/routes');

import errorInterceptor = require('../middlewares/errorInterceptor');
import authenticateMiddleware = require('../middlewares/authenticate');

import type { AppConfig } from './types';

class App {
    readonly port: string;
    readonly host: string;
    readonly config: AppConfig;
    #minioClient: minio.Client;

    #app: express.Application;
    #server: http.Server;

    constructor(config: AppConfig) {
        this.config = config;
        this.port = config.port;
        this.host = config.host;

        this.#minioClient = this.#createMinioClient();

        this.#app = this.#createApp();
        this.#server = this.#createServer();
    }

    #createApp(): express.Application {
        const app = express();

        const authService = new userServices.AuthService(
            userModel,
            this.config.jwt,
            this.#minioClient
        );
        const subsService = new SubscriptionsService(
            subscriptionsModel,
            this.#minioClient
        );
        const profileService = new ProfileService(
            profileModel,
            this.#minioClient
        );
        const postsService = new PostsService(postsModel, this.#minioClient);

        const authController = new userControllers.AuthController(authService);
        const subsController = new SubscriptionsController(subsService);
        const profileController = new ProfileController(profileService);
        const postsController = new PostsController(postsService);

        const authenticate = authenticateMiddleware(authService);

        const authRouter = userRoutes.authRouter(authController, authenticate);
        const subsRouter = subsRoutes(subsController, authenticate);
        const profileRouter = profileRoutes(profileController, authenticate);
        const postsRouter = postsRoutes(postsController, authenticate);

        this.#initAppMiddlewares(app);

        app.use('/api/v1/auth', authRouter);
        app.use('/api/v1/subscriptions', subsRouter);
        app.use('/api/v1/profiles', profileRouter);
        app.use('/api/v1/posts', postsRouter);

        app.use(errorInterceptor);

        return app;
    }

    #createServer(): http.Server {
        const server = http.createServer(this.#app);
        return server;
    }

    #initAppMiddlewares(app: express.Application) {
        let corsOptions = {
            origin: ['http://localhost:5173', 'http://localhost:4173'],
            credentials: true,
        };
        app.use(cors(corsOptions));
        app.use(
            express.urlencoded({
                extended: true,
            })
        );
        app.use(fileUpload());
        app.use(express.json());
        app.use(cookieParser());
        app.use(morgan('dev'));
    }

    #createMinioClient() {
        const minioClient = new minio.Client(this.config.minio);

        return minioClient;
    }

    public start(): void {
        this.#server.listen(this.port, () => {
            console.log(`Running server on http://${this.host}:${this.port}`);
        });
    }
}

export = App;
