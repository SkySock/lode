import express = require('express');
import type { ProfileController } from './types';
import type { AuthMiddleware } from '../types';

const profileRouter = (
    controller: ProfileController,
    authMiddleware: AuthMiddleware
) => {
    const router = express.Router();

    router.get('/', controller.getProfilesTinyInfo);
    router.get('/me/follows', authMiddleware, controller.getFollows);
    router.get('/:username', controller.getProfileInfo);
    router.post('/:username/follow', authMiddleware, controller.follow);
    router.delete('/:username/follow', authMiddleware, controller.unFollow);
    router.get('/:username/follow', authMiddleware, controller.checkFollow);

    return router;
};

export = profileRouter;
