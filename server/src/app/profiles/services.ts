import type { Client } from 'minio';
import type {
    ProfileInfoResponse,
    ProfileModel,
    ProfileTinyInfoResponse,
    ProfileService as PService,
} from './types';
import type { User } from '../../database/types';

class ProfileService implements PService {
    readonly S3_AVATAR_BUCKET = 'user-avatars';

    #profileModel: ProfileModel;
    #minioClient: Client;

    constructor(profileModel: ProfileModel, minioClient: Client) {
        this.#profileModel = profileModel;
        this.#minioClient = minioClient;
    }

    async getProfileInfo(username: string) {
        const profileInfo = await this.#profileModel.getProfileInfoByUsername(
            username
        );

        if (!profileInfo) {
            return;
        }

        let profileResInfo: ProfileInfoResponse = {
            id: profileInfo.id,
            displayName: profileInfo.displayName,
            username: profileInfo.username,
            bio: profileInfo.bio,
            avatarUrl: '',
            followersCount: +profileInfo.followersCount,
        };

        if (profileInfo.avatar.length === 0) {
            return profileResInfo;
        }

        try {
            const avatarUrl = await this.#minioClient.presignedUrl(
                'GET',
                this.S3_AVATAR_BUCKET,
                profileInfo.avatar,
                3600 * 24
            );
            profileResInfo.avatarUrl = avatarUrl;
        } catch (error) {
            console.log(error);
        }

        return profileResInfo;
    }

    async getProfilesTinyInfo(
        usersId: User['id'][]
    ): Promise<ProfileTinyInfoResponse[]> {
        const profiles = await this.#profileModel.getProfilesTinyInfo(usersId);

        if (profiles.length === 0) return [];

        return Promise.all(
            profiles.map(async (p): Promise<ProfileTinyInfoResponse> => {
                try {
                    const avatarUrl = await this.#minioClient.presignedUrl(
                        'GET',
                        this.S3_AVATAR_BUCKET,
                        p.avatar,
                        3600 * 24
                    );

                    return {
                        id: p.id,
                        username: p.username,
                        displayName: p.displayName,
                        avatarUrl: avatarUrl,
                    };
                } catch (error) {
                    return {
                        id: p.id,
                        username: p.username,
                        displayName: p.displayName,
                        avatarUrl: '',
                    };
                }
            })
        );
    }

    async getFollows(userId: string) {
        const followsRaw = await this.#profileModel.getFollows(userId);

        return followsRaw.map((fl) => ({ profileId: fl.fkFollowsUser }));
    }

    async follow(idUser: string, followsUsername: string) {
        return await this.#profileModel.createFollow(idUser, followsUsername);
    }

    async unFollow(idUser: string, followsUsername: string) {
        await this.#profileModel.removeFollow(idUser, followsUsername);
    }

    async checkFollow(
        idUser: string,
        followsUsername: string
    ): Promise<boolean> {
        return !!(await this.#profileModel.getFollow(idUser, followsUsername));
    }
}

export = ProfileService;
