import { type Request, type Response, type NextFunction } from 'express';
import type { ProfileService, ProfileController as PController } from './types';
import errors = require('../../lib/errors');
import { AuthorizedRequest } from '../types';
import validators = require('./validators');
import utils from '../../lib/utils';

class ProfileController implements PController {
    #profileService;

    constructor(profileService: ProfileService) {
        this.#profileService = profileService;
    }

    getProfileInfo = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const username = req.params.username;

        try {
            const profile = await this.#profileService.getProfileInfo(username);
            if (!profile) {
                throw new errors.HttpError(
                    404,
                    'USER_NOT_FOUND',
                    'Profile not found'
                );
            }

            return res.status(200).json(profile);
        } catch (error) {
            if (error instanceof errors.HttpError) {
                return next(error);
            }

            throw error;
        }
    };

    getProfilesTinyInfo = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const profilesId = req.query.profileId;

        if (!validators.getProfilesTinyInfoQueryParamsValidateFn(req.query)) {
            return next(
                new errors.HttpError(
                    400,
                    'QUERY_PARAMS_VALIDATION',
                    utils.ajvErrorsToResponseMessage(
                        validators.getProfilesTinyInfoQueryParamsValidateFn
                            .errors
                    )
                )
            );
        }

        try {
            const profiles = await this.#profileService.getProfilesTinyInfo(
                req.query.profileId
            );

            return res.status(200).json(profiles);
        } catch (error) {
            console.log(error);
            return next(new errors.HttpError(400));
        }
    };

    getFollows = async (req: Request, res: Response, next: NextFunction) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        try {
            const follows = await this.#profileService.getFollows(idUser);
            return res.status(200).json(follows);
        } catch (error) {
            console.log(error);
            return next(new errors.HttpError(500));
        }
    };

    follow = async (req: Request, res: Response, next: NextFunction) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        try {
            const result = await this.#profileService.follow(
                idUser,
                req.params.username
            );

            if (result.length !== 1) {
                return next(new errors.HttpError(404));
            }

            return res.sendStatus(201);
        } catch (error) {
            console.log(error);
        }
        res.sendStatus(400);
    };

    unFollow = async (req: Request, res: Response, next: NextFunction) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        try {
            await this.#profileService.unFollow(idUser, req.params.username);
            return res.sendStatus(204);
        } catch (error) {
            console.log(error);
        }
        res.sendStatus(400);
    };

    checkFollow = async (req: Request, res: Response, next: NextFunction) => {
        const idUser = (req as AuthorizedRequest).userId;
        if (!idUser) {
            throw new Error('Must be authenticated');
        }

        try {
            const isFollow = await this.#profileService.checkFollow(
                idUser,
                req.params.username
            );

            if (isFollow) {
                return res.sendStatus(200);
            }
            return res.sendStatus(404);
        } catch (error) {
            console.log(error);
            return res.sendStatus(500);
        }
    };
}

export = ProfileController;
