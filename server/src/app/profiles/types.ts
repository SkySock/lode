import type { Request, Response, NextFunction } from 'express';
import type { FollowRelations, User } from '../../database/types';

export interface ProfileModel {
    getProfileInfo(id: string): Promise<ProfileInfo | undefined>;
    getProfileInfoByUsername(
        username: string
    ): Promise<ProfileInfo | undefined>;
    createFollow(
        idUser: string,
        followsUsername: string
    ): Promise<{ createdAt: string }[]>;
    removeFollow(idUser: string, followsUsername: string): Promise<number>;
    getFollow(
        idUser: string,
        followsUsername: string
    ): Promise<FollowRelations | undefined>;
    getProfilesTinyInfo(usersId: User['id'][]): Promise<ProfileTinyInfo[]>;
    getFollows(usersId: User['id']): Promise<FollowRelations[]>;
}

export interface ProfileService {
    getProfileInfo(username: string): Promise<ProfileInfoResponse | undefined>;
    follow(
        idUser: string,
        followsUsername: string
    ): Promise<{ createdAt: string }[]>;
    unFollow(idUser: string, followsUsername: string): Promise<void>;
    checkFollow(idUser: string, followsUsername: string): Promise<boolean>;
    getProfilesTinyInfo(
        usersId: User['id'][]
    ): Promise<ProfileTinyInfoResponse[]>;
    getFollows(usersId: User['id']): Promise<ProfileId[]>;
}

export interface ProfileController {
    getProfileInfo: Controller;
    follow: Controller;
    unFollow: Controller;
    checkFollow: Controller;
    getProfilesTinyInfo: Controller;
    getFollows: Controller;
}

export interface ProfileInfo {
    id: string;
    displayName: string;
    username: string;
    avatar: string;
    bio: string;
    followersCount: number;
}

export interface ProfileTinyInfo {
    id: string;
    displayName: string;
    username: string;
    avatar: string;
}

export interface ProfileInfoResponse extends Omit<ProfileInfo, 'avatar'> {
    avatarUrl: string;
}

export type ProfileId = {
    profileId: string;
};

export interface ProfileTinyInfoResponse
    extends Omit<ProfileTinyInfo, 'avatar'> {
    avatarUrl: string;
}

type Controller = (req: Request, res: Response, next: NextFunction) => void;
