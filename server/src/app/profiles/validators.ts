import { type JSONSchemaType } from 'ajv';

import ajv = require('../../lib/ajv');

type GetProfileTinyInfoQueryParams = {
    profileId: string[];
};

const getProfilesTinyInfoQueryParamsSchema: JSONSchemaType<GetProfileTinyInfoQueryParams> =
    {
        type: 'object',
        properties: {
            profileId: {
                type: 'array',
                items: { type: 'string' },
                default: [],
            },
        },

        required: ['profileId'],
        additionalProperties: false,
    };

export = {
    getProfilesTinyInfoQueryParamsValidateFn: ajv.compile(
        getProfilesTinyInfoQueryParamsSchema
    ),
};
