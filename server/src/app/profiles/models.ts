import { type Knex } from 'knex';
import db = require('../../database/db');
import type { FollowRelations, User } from '../../database/types';
import type { ProfileInfo, ProfileModel as PModel } from './types';
import { join } from 'path';

class ProfileModel implements PModel {
    readonly T_USERS = 'users';
    readonly T_FOLLOW_RELATIONS = 'follow_relations';

    async getProfileInfo(id: string) {
        return db
            .select<ProfileInfo>(
                'id',
                'displayName',
                'username',
                'avatar',
                'bio'
            )
            .from<User>(this.T_USERS)
            .where('id', id)
            .first();
    }

    async getProfileInfoByUsername(username: string) {
        return db
            .select<ProfileInfo>(
                'u.id',
                'u.displayName',
                'u.username',
                'u.avatar',
                'u.bio',
                db.raw('count(??) as ??', [
                    'fr.fkFollowsUser',
                    'followersCount',
                ])
            )
            .from<User>({ u: this.T_USERS })
            .where('u.username', username)
            .leftJoin(
                { fr: this.T_FOLLOW_RELATIONS },
                'u.id',
                '=',
                'fr.fkFollowsUser'
            )
            .groupBy('u.id')
            .first();
    }

    async getProfiles(page: number = 1, count: number = 10) {
        return db
            .select<ProfileInfo>(
                'id',
                'displayName',
                'username',
                'avatar',
                'bio'
            )
            .from<User>(this.T_USERS)
            .orderBy('username', 'asc')
            .limit(count)
            .offset((page - 1) * count);
    }

    getProfilesTinyInfo(usersId: User['id'][]) {
        return db<User>(this.T_USERS)
            .select('id', 'avatar', 'displayName', 'username')
            .whereIn('id', usersId)
            .orderBy('username', 'asc');
    }

    getFollows(userId: User['id']) {
        return db<FollowRelations>(this.T_FOLLOW_RELATIONS)
            .select('fkUser', 'fkFollowsUser', 'createdAt')
            .where('fkUser', userId);
    }

    async createFollow(
        idUser: string,
        followsUsername: string
    ): Promise<{ createdAt: string }[]> {
        return db
            .into(
                db.raw('?? (??, ??)', [
                    this.T_FOLLOW_RELATIONS,
                    'fkUser',
                    'fkFollowsUser',
                ])
            )
            .insert(
                db
                    .select(
                        db.raw(`?, ??`, [idUser, { fkFollowsUser: 'u.id' }])
                    )
                    .from({ u: 'users' })
                    .where('u.username', followsUsername),
                ['createdAt']
            );
    }

    async removeFollow(idUser: string, followsUsername: string) {
        return db<FollowRelations>('follow_relations')
            .delete()
            .using(['users'])
            .where('fkUser', idUser)
            .andWhere('users.username', followsUsername)
            .andWhereRaw('"fkFollowsUser" = users.id');
    }

    async getFollow(followerId: string, followingUsername: string) {
        return db
            .select('*')
            .from<FollowRelations>({ f: this.T_FOLLOW_RELATIONS })
            .where('f.fkUser', followerId)
            .andWhere(
                'f.fkFollowsUser',
                db('users').select('id').where('username', followingUsername)
            )
            .first();
    }

    async following(idUser: string) {
        return db
            .select('u.id, u.username, u.displayName, u.avatar')
            .from({ f: this.T_FOLLOW_RELATIONS })
            .join({ u: this.T_USERS }, 'u.id', 'f.fkFollowsUser')
            .where('f.fkUser', idUser);
    }
}

export = new ProfileModel();
