import type { Request, Response, NextFunction } from 'express';
import type { ClientOptions } from 'minio';

import knex = require('knex');

export interface AppConfig {
    port: string;
    host: string;
    jwt: JWTOptions;
    minio: ClientOptions;
    knexConfig: { [key: string]: knex.Knex.Config };
}

export interface JWTOptions {
    secret: string;
    refreshDuration: number;
    accessDuration: number;
}

export interface AuthorizedRequest extends Request {
    userId: string;
}

export type AuthMiddleware = (
    req: Request,
    _: Response,
    next: NextFunction
) => Promise<void>;
