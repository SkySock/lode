import type { AuthService } from '../app/users/types';
import type { Request, Response, NextFunction } from 'express';
import type { AuthorizedRequest } from '../app/types';
import errors = require('../lib/errors');

const authenticate =
    (authService: AuthService) =>
    async (req: Request, _: Response, next: NextFunction) => {
        const authorizationData = req.headers.authorization;
        if (!authorizationData) {
            return next(
                new errors.HttpError(
                    401,
                    'UNAUTHORIZED_REQUEST',
                    'Access Denied / Unauthorized request'
                )
            );
        }

        const token = authorizationData.split(' ');
        if (token[0].toLowerCase() !== 'bearer' || token.length != 2) {
            return next(
                new errors.HttpError(
                    403,
                    'WRONG_CREDENTIALS',
                    'Access Denied / Wrong credentials'
                )
            );
        }

        try {
            const payload = authService.verifyAccessToken(token[1]);
            (req as AuthorizedRequest).userId = payload.sub;
        } catch (error) {
            return next(
                new errors.HttpError(
                    401,
                    'UNAUTHORIZED_REQUEST',
                    'Access Denied / Unauthorized request'
                )
            );
        }

        return next();
    };

export = authenticate;
