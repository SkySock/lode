import type { NextFunction, Request, Response } from 'express';
import errors from '../lib/errors';

const errorInterceptor = (
    err: any,
    req: Request,
    res: Response,
    next: NextFunction
) => {
    if (err instanceof errors.HttpError) {
        return res
            .status(err.status)
            .json({ code: err.code, message: err.message });
    }

    next(err);
};

export = errorInterceptor;
