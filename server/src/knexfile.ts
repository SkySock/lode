import knex = require('knex');
import appConfig = require('./config');

type Config = knex.Knex.Config;

const config: { [key: string]: Config } = appConfig.knexConfig;

export = config;
