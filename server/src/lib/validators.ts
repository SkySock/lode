import type { UploadedFile } from 'express-fileupload';
import utils = require('./utils');

const IMAGE_EXTENSIONS = {
    png: 'image/png',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    gif: 'image/gif',
} as const;

function validateImageFile(
    file: UploadedFile,
    allowedExtensions: ImageExtension[] = Object.keys(
        IMAGE_EXTENSIONS
    ) as ImageExtension[],
    maxSize?: number
) {
    const file_extension = utils.getFileExtension(file);

    if (
        !(allowedExtensions as string[]).includes(file_extension) ||
        !(
            IMAGE_EXTENSIONS[file_extension as ImageExtension] ===
            (file.mimetype as ImageMimeTypes)
        )
    ) {
        return false;
    }

    if (maxSize && file.size > maxSize) {
        return false;
    }

    return true;
}

type ImageExtension = keyof typeof IMAGE_EXTENSIONS;
type ImageMimeTypes = (typeof IMAGE_EXTENSIONS)[ImageExtension];

export = { validateImageFile };
