import Ajv from 'ajv';
import addFormats from 'ajv-formats';

const ajv = new Ajv({ useDefaults: true, coerceTypes: 'array' });

addFormats(ajv, ['email']);

export = ajv;
