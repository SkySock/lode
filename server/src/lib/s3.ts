import type { Client } from 'minio';

async function createBucket(minioClient: Client, bucketName: string) {
    const exists = await minioClient.bucketExists(bucketName);

    if (!exists) {
        await minioClient.makeBucket(bucketName);
    }
}

export = { createBucket };
