import crypto = require('crypto');
import argon2 = require('argon2');

const hashingConfig: argon2.Options = {
    // based on OWASP cheat sheet recommendations on June 2024
    memoryCost: 19456,
    parallelism: 1,
    timeCost: 2,
};

async function hashPassword(password: string) {
    let salt = crypto.randomBytes(32);

    return argon2.hash(password, {
        ...hashingConfig,
        salt,
    });
}

async function verifyPassword(password: string, hash: string) {
    return argon2.verify(hash, password, hashingConfig);
}

export = { hashPassword, verifyPassword };
