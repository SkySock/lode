class HttpError extends Error {
    readonly #code: string;
    readonly #status: number;
    constructor(
        status = 500,
        code: HttpErrorCode = 'GENERIC',
        ...params: any[]
    ) {
        super(...params);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, HttpError);
        }

        this.#code = code;
        this.#status = status;
    }

    get code() {
        return this.#code;
    }

    get status() {
        return this.#status;
    }
}

class ServiceError extends Error {
    readonly #code: string;

    constructor(code = 'GENERIC', ...params: any[]) {
        super(...params);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, ServiceError);
        }

        this.#code = code;
    }

    public get code(): string {
        return this.#code;
    }
}

const httpErrorCodes = [
    'GENERIC',
    'NOT_FOUND',
    'USER_NOT_FOUND',
    'WRONG_CREDENTIALS',
    'UNAUTHORIZED_REQUEST',
    'BODY_VALIDATION',
    'QUERY_PARAMS_VALIDATION',
    'USER_EXISTS',
    'FILE_VALIDATION',
    'SUBSCRIPTION_PRICE_EXISTS',
] as const;

type HttpErrorCode = (typeof httpErrorCodes)[number];

export = { HttpError, ServiceError };
