import type { ErrorObject } from 'ajv';
import type { UploadedFile } from 'express-fileupload';

function getEnv(name: string, defaultValue?: string) {
    const env = process.env[name];

    if (typeof env === 'string') return env;

    if (typeof defaultValue === 'undefined') {
        throw Error(`${name} environment not found`);
    }

    return defaultValue;
}

function getErrorMessage(e: unknown) {
    let error: string = '';
    if (typeof e === 'string') {
        error = e;
    } else if (e instanceof Error) {
        error = e.message;
    }
    return error;
}

function ajvErrorsToResponseMessage(err?: null | ErrorObject[]) {
    if (err) {
        return err.map((e) => e.message).join(';');
    }

    return '';
}

function getFileExtension(file: UploadedFile) {
    return file.name.slice(((file.name.lastIndexOf('.') - 1) >>> 0) + 2);
}

export = {
    getEnv,
    getErrorMessage,
    ajvErrorsToResponseMessage,
    getFileExtension,
};
