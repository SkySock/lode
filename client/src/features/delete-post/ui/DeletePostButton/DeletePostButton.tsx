import { FC, ReactNode } from 'react';
import classes from './DeletePostButton.module.css';

interface DeletePostButtonProps {
  children?: ReactNode;
}

export const DeletePostButton: FC<DeletePostButtonProps> = ({ children }) => {
  return <button className={classes.btn}>{children}</button>;
};
