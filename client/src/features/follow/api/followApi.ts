import { api } from '../../../shared/api';

export const followApi = {
  follow: (username: string) => {
    return api.post(`/profiles/${username}/follow`);
  },
  unFollow: (username: string) => {
    return api.delete(`/profiles/${username}/follow`);
  },
  checkFollow: (username: string) => {
    return api.get(`/profiles/${username}/follow`);
  },
};
