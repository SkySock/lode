import classNames from 'classnames/bind';
import { FC, useEffect, useState } from 'react';
import { followApi } from '../../api';

import classes from './FollowButton.module.css';

interface FollowButtonProps {
  followOnUsername: string;
  onFollow?: () => void;
  onRemoveFollow?: () => void;
}

const cx = classNames.bind(classes);

export const FollowButton: FC<FollowButtonProps> = ({
  followOnUsername,
  onFollow,
  onRemoveFollow,
}) => {
  const [isFollow, setIsFollow] = useState(false);

  useEffect(() => {
    followApi
      .checkFollow(followOnUsername)
      .then(() => {
        setIsFollow(true);
      })
      .catch(() => {
        setIsFollow(false);
      });
  }, [followOnUsername]);

  const followHandler = () => {
    followApi.follow(followOnUsername).then(() => {
      setIsFollow(true);
      onFollow?.();
    });
  };

  const unFollowHandler = () => {
    followApi.unFollow(followOnUsername).then(() => {
      setIsFollow(false);
      onRemoveFollow?.();
    });
  };

  const className = cx({
    btn: true,
    followBtn: !isFollow,
    unFollowBtn: isFollow,
  });

  return (
    <>
      {isFollow && (
        <button className={className} onClick={unFollowHandler}>
          Перестать отслеживать
        </button>
      )}
      {!isFollow && (
        <button className={className} onClick={followHandler}>
          Отслеживать
        </button>
      )}
    </>
  );
};
