import { useNavigate } from '@tanstack/react-router';
import { ChangeEventHandler, FC, FormEventHandler, useState } from 'react';
import { useViewerStore } from '../../../../entities/viewer';
import { CreatePostData, postsApi } from '../../../../shared/api';
import {
  InputFile,
  ModalWindow,
  PrimaryButton,
  TextArea,
} from '../../../../shared/ui';

import classes from './PostCreateForm.module.css';

interface PostCreateFormProps {
  onClose: () => void;
}

interface FormData {
  description: string;
  tags: string;
  mediaFiles: FileList | null;
}

const defaultFormData: FormData = {
  description: '',
  tags: '',
  mediaFiles: null,
};

export const PostCreateForm: FC<PostCreateFormProps> = ({ onClose }) => {
  const [formData, setFormData] = useState(defaultFormData);
  const viewer = useViewerStore((state) => state.user);
  const navigate = useNavigate();

  const handleChangesData =
    <K extends keyof FormData>(
      key: K
    ): ChangeEventHandler<
      K extends keyof Pick<FormData, 'description' | 'tags'>
        ? HTMLTextAreaElement
        : HTMLInputElement
    > =>
    (e) => {
      switch (key) {
        case 'mediaFiles':
          setFormData({ ...formData, [key]: e.target.files });
          break;
        case 'tags':
          setFormData({ ...formData, [key]: e.target.value.toLowerCase() });
          break;
        default:
          setFormData({ ...formData, [key]: e.target.value });
      }
    };

  const handleOnSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    if (!formData.mediaFiles) return;

    const tagsStr = formData.tags.trim();

    const postData: CreatePostData = {
      description: formData.description,
      mediaFiles: formData.mediaFiles,
    };
    if (tagsStr.length > 0) {
      postData.tags = tagsStr.split(' ');
    }
    console.log(postData);

    postsApi.createPost(postData).then(({ data }) => {
      console.log(data);
      onClose();
      if (!viewer) return;
      navigate({
        to: '/profiles/$username/posts/$postId',
        params: { username: viewer?.username, postId: data.id },
      });
    });
  };

  return (
    <ModalWindow title="Создать публикацию" onClose={onClose}>
      <form className={classes.form} onSubmit={handleOnSubmit}>
        <div className={classes.images}></div>
        <InputFile
          accept="image/png, image/jpeg"
          multiple={true}
          onChange={handleChangesData('mediaFiles')}
        />
        <TextArea
          label="*Описание:"
          value={formData.description}
          onChange={handleChangesData('description')}
        />
        <TextArea
          label="Теги (перечислите теги разделяя пробелом - ' '):"
          value={formData.tags}
          onChange={handleChangesData('tags')}
        />
        <PrimaryButton type="submit">Опубликовать</PrimaryButton>
      </form>
    </ModalWindow>
  );
};
