import { api } from '../../../shared/api';

export const signOut = () => api.post('/auth/sign-out');
