import { FC, ReactNode } from 'react';
import { useViewerStore } from '../../../entities/viewer';
import { signOut } from '../api';

interface SignOutButtonProps {
  children?: ReactNode;
  className: string;
}

export const SignOutButton: FC<SignOutButtonProps> = ({
  children,
  className,
}) => {
  const setViewer = useViewerStore((state) => state.setUser);

  const onClickHandler = () => {
    signOut().finally(() => {
      setViewer();
      localStorage.removeItem('accessToken');
    });
  };

  return (
    <button className={className} onClick={onClickHandler}>
      {children}
    </button>
  );
};
