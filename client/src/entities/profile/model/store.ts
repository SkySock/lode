import { create } from 'zustand';
import { Profile } from '../../../shared/api';

interface ProfileState {
  profile: Profile;
  setProfile: (profile: Profile) => void;
  incFollowers: () => void;
  decFollowers: () => void;
}

const defaultProfile: Profile = {
  id: '',
  username: '',
  displayName: '',
  bio: '',
  followersCount: 0,
  avatarUrl: '',
};

export const useProfileStore = create<ProfileState>()((set) => ({
  profile: defaultProfile,
  setProfile: (profile: Profile) => set(() => ({ profile })),
  incFollowers: () =>
    set((state) => ({
      profile: {
        ...state.profile,
        followersCount: state.profile.followersCount + 1,
      },
    })),
  decFollowers: () =>
    set((state) => ({
      profile: {
        ...state.profile,
        followersCount: state.profile.followersCount - 1,
      },
    })),
}));
