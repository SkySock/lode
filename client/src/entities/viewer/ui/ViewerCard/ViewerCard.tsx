import { Link } from '@tanstack/react-router';
import { FC } from 'react';
import { Viewer } from '../../model';
import { ViewerAvatar } from '../ViewerAvatar';
import classes from './ViewerCard.module.css';

interface ViewerCardProps {
  user: Viewer;
}

export const ViewerCard: FC<ViewerCardProps> = ({ user }) => {
  return (
    <div className={classes.card}>
      <ViewerAvatar user={user} className={classes.avatar} />
      <div className={classes.nameWrapper}>
        <h2 className={classes.displayName}>{user.displayName}</h2>
        <Link to="/profiles/$username" params={{ username: user.username }}>
          @{user.username}
        </Link>
      </div>
    </div>
  );
};
