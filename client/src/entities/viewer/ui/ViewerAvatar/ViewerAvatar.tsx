import { FC, MouseEventHandler, RefObject } from 'react';
import profileIcon from '../../../../shared/assets/profile.svg';
import { Viewer } from '../../model';

import classes from './ViewerAvatar.module.css';

interface ViewerAvatarProps {
  user: Viewer;
  ref?: RefObject<HTMLDivElement>;
  className?: string;
  onClick?: MouseEventHandler<HTMLDivElement>;
}

export const ViewerAvatar: FC<ViewerAvatarProps> = (props) => {
  return (
    <div
      className={props.className || classes.avatar}
      ref={props.ref}
      onClick={props.onClick}
    >
      <img src={props.user.avatarUrl || profileIcon} className={classes.img} />
    </div>
  );
};
