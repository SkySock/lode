import { create } from 'zustand';
import { getViewerInfo } from '../api';

export interface Viewer {
  id: string;
  username: string;
  displayName: string;
  avatarUrl: string;
}

interface ViewerState {
  user?: Viewer;
  setUser: (user?: Viewer) => void;
  syncUser: () => void;
}

function parseViewer() {
  const viewerStr = localStorage.getItem('viewer');

  if (viewerStr) {
    return JSON.parse(viewerStr) as Viewer;
  }
}

export const useViewerStore = create<ViewerState>()((set) => ({
  user: parseViewer(),
  setUser: (user) =>
    set(() => {
      if (!user) {
        localStorage.removeItem('viewer');
      } else {
        localStorage.setItem('viewer', JSON.stringify(user));
      }
      return { user: user };
    }),
  syncUser: () => {
    getViewerInfo()
      .then(({ data }) => {
        set(() => {
          localStorage.setItem('viewer', JSON.stringify(data));
          return { user: data };
        });
      })
      .catch(() => {
        set(() => ({
          user: undefined,
        }));
      });
  },
}));
