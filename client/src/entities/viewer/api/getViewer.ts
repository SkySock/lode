import { api } from '../../../shared/api';
import { Viewer } from '../model';

export const getViewerInfo = () => {
  return api.get<Viewer>('/auth/me');
};
