import { createFileRoute } from '@tanstack/react-router';
import { z } from 'zod';
import { AuthPage } from '../../pages/auth';

const AUTH_TYPES = ['sign-up', 'sign-in'] as const;

const authSearchSchema = z.object({
  type: z.enum(AUTH_TYPES).catch('sign-in'),
});

// type AuthSearch = z.infer<typeof authSearchSchema>;

export const Route = createFileRoute('/auth')({
  component: AuthPage,
  validateSearch: authSearchSchema,
});
