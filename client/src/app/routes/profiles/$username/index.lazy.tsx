import { createLazyFileRoute } from '@tanstack/react-router';
import { ProfilePage } from '../../../../pages/profile';

export const Route = createLazyFileRoute('/profiles/$username/')({
  component: ProfilePage,
});
