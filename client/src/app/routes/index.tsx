import { createFileRoute } from '@tanstack/react-router';

export const Route = createFileRoute('/')({
  component: Feed,
});

function Feed() {
  return <div>Feed</div>;
}
