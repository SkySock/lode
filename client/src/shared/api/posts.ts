import { api } from './client';

interface PostsList {
  posts: Post[];
  page: number;
}

export interface Post {
  id: string;
  fkAuthor: string;
  fkSubscription: string | null;
  status: string;
  description: string;
  tags: Tag[];
  mediaContents: MediaContent[];
  publishedAt: string;
}

export interface Tag {
  id: string;
  name: string;
}

export interface MediaContent {
  id: string;
  fileSrc: string;
}

export interface CreatePostData {
  mediaFiles: FileList;
  tags?: string[];
  description: string;
}

export const postsApi = {
  getProfilePosts: (authorId: string, page: number = 1, count: number = 15) => {
    return api.get<PostsList>(`/posts`, {
      params: { authorId: authorId, page, count },
    });
  },
  createPost: (postData: CreatePostData) => {
    return api.post<{ id: string }>('/posts', postData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      formSerializer: {},
    });
  },
  getPostInfo: (postId: string) => {
    return api.get<Post>(`/posts/${postId}`);
  },
  deletePost: (postId: string) => {
    return api.delete(`/posts/${postId}`);
  },
};
