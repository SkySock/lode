import axios from 'axios';
import { apiBaseUrl } from '../config';
import { parseJwt } from '../lib';

const TIMEOUT = 3000;

export const api = axios.create({
  baseURL: apiBaseUrl,
  timeout: TIMEOUT,
  withCredentials: true,
  paramsSerializer: {
    indexes: null,
  },
});

api.interceptors.request.use(async (config) => {
  const accessToken = localStorage.getItem('accessToken');

  if (accessToken) {
    const payload = parseJwt(accessToken) as ExpPayload;

    const now = Math.floor(Date.now() / 1000);

    if (payload.exp - 3 < now) {
      localStorage.removeItem('accessToken');

      try {
        const { data } = await api.get<AccessToken>('/auth/access');
        localStorage.setItem('accessToken', data.access);

        config.headers.Authorization = `Bearer ${data.access}`;
      } catch (e) {
        console.log(e);
      }
    } else {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
  } else {
    delete config.headers.Authorization;
  }

  return config;
});

interface AccessToken {
  access: string;
}

interface ExpPayload {
  exp: number;
}
