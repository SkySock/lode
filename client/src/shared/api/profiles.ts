import { api } from './client';

export interface Profile {
  id: string;
  username: string;
  displayName: string;
  avatarUrl: string;
  bio: string;
  followersCount: number;
}

export type ProfileTinyInfo = Pick<
  Profile,
  'id' | 'username' | 'displayName' | 'avatarUrl'
>;

export type ProfileId = {
  profileId: string;
};

export const profilesApi = {
  getProfileByUsername: (username: string) => {
    return api.get<Profile>(`/profiles/${username}`);
  },
  getProfilesInfo: (profileId: string[]) => {
    return api.get<ProfileTinyInfo[]>('/profiles', {
      params: { profileId: profileId },
    });
  },
  getFollows: () => {
    return api.get<ProfileId[]>('/profiles/me/follows');
  },
};
