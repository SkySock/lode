import { create } from 'zustand';

interface AccessTokenState {
  token: string | null;
  set: (newToken: string) => void;
  clear: () => void;
}

export const useAuthStore = create<AccessTokenState>()((set) => ({
  token: localStorage.getItem('accessToken'),
  set: (newToken: string) =>
    set(() => {
      localStorage.setItem('accessToken', newToken);

      return { token: newToken };
    }),
  clear: () => {
    localStorage.removeItem('accessToken');

    return { token: null };
  },
}));

export const setAuthToken = (state: AccessTokenState) => state.set;
export const clearAuthToken = (state: AccessTokenState) => state.clear;
export const getAuthToken = (state: AccessTokenState) => state.token;
