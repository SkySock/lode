import { FC, ReactNode } from 'react';
import classes from './Tag.module.css';

interface TagProps {
  children: ReactNode;
}

export const Tag: FC<TagProps> = ({ children }) => {
  return <div className={classes.tag}>{children}</div>;
};
