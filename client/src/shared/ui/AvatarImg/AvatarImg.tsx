import { FC } from 'react';
import classes from './AvatarImg.module.css';

interface AvatarImgProps {
  src?: string;
  className?: string;
}

export const AvatarImg: FC<AvatarImgProps> = ({ src, className }) => {
  return (
    <div className={className || classes.avatar}>
      <img className={classes.img} src={src} alt="Profile avatar" />
    </div>
  );
};
