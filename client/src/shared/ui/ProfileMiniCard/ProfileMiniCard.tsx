import { Link } from '@tanstack/react-router';
import { FC } from 'react';
import { AvatarImg } from '../AvatarImg';

import classes from './ProfileMiniCard.module.css';

interface ProfileMiniCardProps {
  username: string;
  displayName: string;
  avatarUrl: string;
}

export const ProfileMiniCard: FC<ProfileMiniCardProps> = ({
  username,
  displayName,
  avatarUrl,
}) => {
  return (
    <div className={classes.card}>
      <AvatarImg className={classes.img} src={avatarUrl} />
      <div className={classes.name}>
        <h2>{displayName}</h2>
        <Link to="/profiles/$username" params={{ username: username }}>
          @{username}
        </Link>
      </div>
    </div>
  );
};
