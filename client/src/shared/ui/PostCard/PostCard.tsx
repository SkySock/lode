import { Link } from '@tanstack/react-router';
import { FC } from 'react';
import { Post } from '../../api';
import classes from './PostCard.module.css';

interface PostCardProps {
  post: Post;
}

export const PostCard: FC<PostCardProps> = ({ post }) => {
  return (
    <Link
      to={'/profiles/$username/posts/$postId'}
      params={(prev) => ({
        ...prev,
        username: prev.username || '',
        postId: post.id,
      })}
      className={classes.post}
    >
      <div className={classes.imgContainer}>
        <img className={classes.img} src={post.mediaContents[0]?.fileSrc} />
      </div>
      <div className={classes.descWrapper}>
        <div className={classes.descText}>{post.description}</div>
      </div>
    </Link>
  );
};
