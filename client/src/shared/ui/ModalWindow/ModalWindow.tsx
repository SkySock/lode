import { ReactNode, useEffect, useRef } from 'react';
import classes from './ModalWindow.module.css';

interface ModalProps {
  title?: string;
  children: ReactNode[] | ReactNode;
  onClose?: () => void;
}

export function ModalWindow({ children, title, onClose }: ModalProps) {
  const rootRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleWrapperClick = (event: MouseEvent) => {
      const { target } = event;

      if (target instanceof Node && rootRef.current === target) {
        onClose?.();
      }
    };

    const handleEscapePress = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose?.();
      }
    };

    window.addEventListener('mousedown', handleWrapperClick);
    window.addEventListener('keydown', handleEscapePress);

    return () => {
      window.removeEventListener('mousedown', handleWrapperClick);
      window.removeEventListener('keydown', handleEscapePress);
    };
  }, [onClose]);

  return (
    <div className={classes.screen} ref={rootRef}>
      <div className={classes.window}>
        {!!title && <h2>{title}</h2>}
        {children}
      </div>
    </div>
  );
}
