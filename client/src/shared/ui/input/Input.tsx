import classNames from 'classnames/bind';
import React, { FC } from 'react';
import classes from './Input.module.css';

type InputType = 'text' | 'password' | 'email' | 'url';

interface InputProps {
  value: string;
  type?: InputType;
  errorMessage?: string;
  placeholder?: string;
  label?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  onCopy?: React.ClipboardEventHandler<HTMLInputElement>;
}

const cx = classNames.bind(classes);

export const Input: FC<InputProps> = (props) => {
  const {
    value,
    type = 'text',
    errorMessage,
    placeholder = '',
    label,
    onChange,
    onCopy,
  } = props;

  const inputStyles = cx({ inputField: true, inputError: !!errorMessage });

  return (
    <div className={classes.inputWrapper}>
      {label}
      <input
        className={inputStyles}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onCopy={onCopy}
        type={type}
      />
      {!!errorMessage && (
        <div className={classes.errorMessage}>{errorMessage}</div>
      )}
    </div>
  );
};
