import classNames from 'classnames/bind';
import { FC } from 'react';

import classes from './Input.module.css';

interface TextAreaProps {
  value: string;
  errorMessage?: string;
  placeholder?: string;
  label?: string;
  onChange?: React.ChangeEventHandler<HTMLTextAreaElement>;
  onCopy?: React.ClipboardEventHandler<HTMLTextAreaElement>;
}

const cx = classNames.bind(classes);

export const TextArea: FC<TextAreaProps> = ({
  value,
  errorMessage,
  placeholder = '',
  label,
  onChange,
  onCopy,
}) => {
  const inputStyles = cx({
    inputField: true,
    textarea: true,
    inputError: !!errorMessage,
  });

  return (
    <div className={classes.inputWrapper}>
      {label}
      <textarea
        className={inputStyles}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        onCopy={onCopy}
      />
      {!!errorMessage && (
        <div className={classes.errorMessage}>{errorMessage}</div>
      )}
    </div>
  );
};
