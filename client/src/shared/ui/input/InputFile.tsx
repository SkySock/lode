import classNames from 'classnames/bind';
import { FC, useId, useState } from 'react';
import classes from './Input.module.css';

interface InputFileProps {
  errorMessage?: string;
  label?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  accept?: string;
  multiple?: boolean;
}

const cx = classNames.bind(classes);

export const InputFile: FC<InputFileProps> = ({
  accept,
  onChange,
  errorMessage,
  multiple,
}) => {
  const id = useId();
  const [files, setFile] = useState<File[]>([]);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    if (!e.target.files) return;

    const files = Array.from(e.target.files);
    setFile(files);
    onChange?.(e);
  };

  const wrapperStyles = cx({ inputWrapper: true, error: !!errorMessage });

  return (
    <label htmlFor={id} className={wrapperStyles}>
      <span className={classes.inputFileText}>
        {files.map((f) => f.name).join(', ')}
      </span>
      <input
        id={id}
        className={classes.inputFile}
        onChange={handleChange}
        type="file"
        accept={accept}
        multiple={multiple}
      />
      <span className={classes.inputFileBtn}>
        {!multiple ? 'Выберите файл' : 'Выберите файлы'}
      </span>
      {!!errorMessage && (
        <div className={classes.errorMessage}>{errorMessage}</div>
      )}
    </label>
  );
};
