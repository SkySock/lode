import { type FC } from 'react';
import classes from './Counter.module.css';

interface CounterProps {
  value: number;
  label?: string;
}

export const Counter: FC<CounterProps> = ({ value, label }) => {
  return (
    <div className={classes.counterWrapper}>
      <div className={classes.counter}>{value}</div>
      {label}
    </div>
  );
};
