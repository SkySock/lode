import { FC, ReactNode } from 'react';
import classes from './PrimaryButton.module.css';

interface PrimaryButtonProps {
  type?: 'submit' | 'reset' | 'button';
  onClick?: () => void;
  disabled?: boolean;
  children?: ReactNode;
}

export const PrimaryButton: FC<PrimaryButtonProps> = ({
  children,
  type,
  disabled,
  onClick,
}) => {
  return (
    <button
      className={classes.btn}
      type={type}
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
