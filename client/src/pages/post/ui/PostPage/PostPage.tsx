import { getRouteApi } from '@tanstack/react-router';
import classNames from 'classnames';
import { FC, useEffect, useState } from 'react';
import {
  type Post,
  type ProfileTinyInfo,
  postsApi,
  profilesApi,
} from '../../../../shared/api';
import { ModalWindow, ProfileMiniCard, Tag } from '../../../../shared/ui';

import classes from './PostPage.module.css';

const routeApi = getRouteApi('/profiles/$username/posts/$postId');

export const PostPage: FC = () => {
  const { postId } = routeApi.useParams();
  const navigate = routeApi.useNavigate();
  const [post, setPost] = useState<Post>();
  const [author, setAuthor] = useState<ProfileTinyInfo>();
  const [pictureIndex, setPictureIndex] = useState(0);

  useEffect(() => {
    postsApi.getPostInfo(postId).then(({ data }) => {
      setPost(data);
    });
  }, [postId]);

  useEffect(() => {
    if (!post) return;

    profilesApi.getProfilesInfo([post.fkAuthor]).then(({ data }) => {
      setAuthor(data[0]);
    });
  }, [post]);

  const closeHandler = () => {
    navigate({ to: '/profiles/$username' });
  };

  const nextPictureHandler = () => {
    if (!post) return;

    setPictureIndex((pictureIndex + 1) % post.mediaContents.length);
  };

  const prevPictureHandler = () => {
    if (!post || pictureIndex === 0) {
      return;
    }

    setPictureIndex((pictureIndex - 1) % post.mediaContents.length);
  };

  return (
    <ModalWindow onClose={closeHandler}>
      {!!post && (
        <div className={classes.postWrapper}>
          <div className={classes.mediaContent}>
            <div
              className={classNames(classes.leftSide, classes.clickSide)}
              onClick={prevPictureHandler}
            />
            <div
              className={classNames(classes.rightSide, classes.clickSide)}
              onClick={nextPictureHandler}
            />
            <img
              className={classes.picture}
              src={post.mediaContents[pictureIndex].fileSrc}
            />
          </div>
          <div className={classes.infoWrapper}>
            {!!author && (
              <div className={classes.author}>
                <ProfileMiniCard {...author} />
              </div>
            )}
            <div className={classes.description}>{post.description}</div>
            <div className={classes.tagsContainer}>
              {post.tags.map((tag) => (
                <Tag key={tag.id}>{tag.name}</Tag>
              ))}
            </div>
          </div>
        </div>
      )}
    </ModalWindow>
  );
};
