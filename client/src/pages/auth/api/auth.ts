import { api } from '../../../shared/api';

interface SignInDTO {
  username: string;
  password: string;
}

interface SignUpDTO {
  username: string;
  displayName: string;
  password: string;
  email: string;
  avatar?: File;
}

interface AccessToken {
  access: string;
}

export const authApi = {
  signIn: (data: SignInDTO) => {
    return api.post<AccessToken>('/auth/refresh', data);
  },
  signUp: (data: SignUpDTO) => {
    return api.post('/auth/register', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};
