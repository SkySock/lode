import { Link, useNavigate } from '@tanstack/react-router';
import { ChangeEventHandler, FC, FormEventHandler, useState } from 'react';
import { Input, InputFile, PrimaryButton } from '../../../../shared/ui';
import { authApi } from '../../api';
import classes from './SignUpForm.module.css';

type FormData = {
  displayName: string;
  username: string;
  avatar?: File;
  email: string;
  password: string;
};

type FormDataKey = keyof FormData;

const defaultFormData: FormData = {
  displayName: '',
  username: '',
  avatar: undefined,
  email: '',
  password: '',
};

export const SignUpForm: FC = () => {
  const [formData, setFormData] = useState(defaultFormData);
  const navigate = useNavigate({ from: '/auth' });

  const handleChangesData =
    (key: FormDataKey): ChangeEventHandler<HTMLInputElement> =>
    (e) => {
      switch (key) {
        case 'avatar':
          setFormData({ ...formData, avatar: e.target.files?.[0] });
          break;
        default:
          setFormData({ ...formData, [key]: e.target.value });
      }
    };

  const handleOnSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    authApi.signUp(formData).then(({ data }) => {
      console.log(data);
      navigate({ to: '/auth', search: { type: 'sign-in' } });
    });
  };

  return (
    <>
      <form className={classes.form} onSubmit={handleOnSubmit}>
        <Input
          value={formData.displayName}
          onChange={handleChangesData('displayName')}
          placeholder="John Doe"
        />
        <InputFile
          onChange={handleChangesData('avatar')}
          accept="image/png, image/jpeg"
        />
        <Input
          value={formData.username}
          onChange={handleChangesData('username')}
          placeholder="username"
        />
        <Input
          type="email"
          value={formData.email}
          onChange={handleChangesData('email')}
          placeholder="john@doe.com"
        />
        <Input
          type="password"
          value={formData.password}
          onChange={handleChangesData('password')}
          placeholder="password"
        />

        <PrimaryButton type="submit">Зарегистрироваться</PrimaryButton>
      </form>
      <Link to="/auth" search={{ type: 'sign-in' }}>
        Войти
      </Link>
    </>
  );
};
