import { Link } from '@tanstack/react-router';
import { ChangeEventHandler, FC, FormEventHandler, useState } from 'react';
import { useViewerStore, viewerApi } from '../../../../entities/viewer';
import { Input, PrimaryButton } from '../../../../shared/ui';
import { authApi } from '../../api';

import classes from './SignInForm.module.css';

const defaultFormData = {
  username: '',
  password: '',
};

type FormDataKey = keyof typeof defaultFormData;

export const SignInFrom: FC = () => {
  const [formData, setFormData] = useState(defaultFormData);
  const setViewer = useViewerStore((state) => state.setUser);

  const handleChangesData =
    (key: FormDataKey): ChangeEventHandler<HTMLInputElement> =>
    (e) => {
      setFormData({ ...formData, [key]: e.target.value });
    };

  const handleOnSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    authApi.signIn(formData).then((res) => {
      const { data } = res;

      localStorage.setItem('accessToken', data.access);

      viewerApi.getViewerInfo().then(({ data }) => {
        setViewer(data);
      });
    });
  };

  return (
    <>
      <form className={classes.form} onSubmit={handleOnSubmit}>
        <Input
          value={formData.username}
          onChange={handleChangesData('username')}
          type="text"
          placeholder="username"
        />
        <Input
          value={formData.password}
          onChange={handleChangesData('password')}
          type="password"
          placeholder="password"
        />
        <PrimaryButton type="submit">Войти</PrimaryButton>
      </form>
      <Link to="/auth" search={{ type: 'sign-up' }}>
        Зарегистрироваться
      </Link>
    </>
  );
};
