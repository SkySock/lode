import { getRouteApi, Navigate, useNavigate } from '@tanstack/react-router';
import { FC } from 'react';
import { useViewerStore } from '../../../../entities/viewer';
import { ModalWindow } from '../../../../shared/ui';
import { SignInFrom } from '../SignInForm';
import { SignUpForm } from '../SignUpForm';

const routeApi = getRouteApi('/auth');

const getTitle = (authType: 'sign-in' | 'sign-up') => {
  switch (authType) {
    case 'sign-in':
      return 'Вход';
    case 'sign-up':
      return 'Регистрация';
  }
};

export const AuthPage: FC = () => {
  const { type } = routeApi.useSearch();
  const viewer = useViewerStore((state) => state.user);
  const navigate = useNavigate({ from: '/auth' });

  const closeHandler = () => {
    navigate({ to: '/' });
  };

  return (
    <>
      {!!viewer && <Navigate to="/" />}
      <ModalWindow title={getTitle(type)} onClose={closeHandler}>
        {type === 'sign-in' && <SignInFrom />}
        {type === 'sign-up' && <SignUpForm />}
      </ModalWindow>
    </>
  );
};
