import { Outlet } from '@tanstack/react-router';
import React from 'react';
import { FC } from 'react';
import { Header } from '../../../widgets/header';
import { NavBar } from '../../../widgets/nav-bar';
import classes from './RootPage.module.css';

const TanStackRouterDevtools =
  process.env.NODE_ENV === 'production'
    ? () => null
    : React.lazy(() =>
        import('@tanstack/router-devtools').then((res) => ({
          default: res.TanStackRouterDevtools,
        }))
      );

export const RootPage: FC = () => {
  return (
    <>
      <Header />
      <div className={classes.pageWrapper}>
        <NavBar />
        <Outlet />
      </div>

      <TanStackRouterDevtools />
    </>
  );
};
