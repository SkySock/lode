import { getRouteApi } from '@tanstack/react-router';
import { FC, useEffect } from 'react';
import { useProfileStore } from '../../../../entities/profile';
import { profilesApi } from '../../../../shared/api';
import { PostsContainer } from '../../../../widgets/posts';
import { ProfileInfo } from '../../../../widgets/profile';
import classes from './ProfilePage.module.css';

const routeApi = getRouteApi('/profiles/$username/');

export const ProfilePage: FC = () => {
  const { username } = routeApi.useParams();
  const setProfile = useProfileStore((state) => state.setProfile);

  useEffect(() => {
    profilesApi
      .getProfileByUsername(username)
      .then(({ data }) => setProfile(data));
  }, [setProfile, username]);

  return (
    <div className={classes.pageWrapper}>
      <ProfileInfo />
      <PostsContainer />
    </div>
  );
};
