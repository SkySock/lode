import { FC, useEffect, useState } from 'react';
import { useProfileStore } from '../../../../entities/profile';
import { type Post, postsApi } from '../../../../shared/api';
import { PostCard } from '../../../../shared/ui';

import classes from './PostsContainer.module.css';

export const PostsContainer: FC = () => {
  const profile = useProfileStore((state) => state.profile);
  const [posts, setPosts] = useState<Post[]>([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    if (profile.id) {
      postsApi.getProfilePosts(profile.id, page).then(({ data }) => {
        setPosts(data.posts);
      });
    }
  }, [profile.id, page]);

  return (
    <div className={classes.postsWrapper}>
      {posts.map((post) => (
        <PostCard post={post} key={post.id} />
      ))}
    </div>
  );
};
