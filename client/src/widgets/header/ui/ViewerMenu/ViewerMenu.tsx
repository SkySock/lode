import { FC, MouseEventHandler, useState } from 'react';
import { Viewer, ViewerAvatar, ViewerCard } from '../../../../entities/viewer';
import { SignOutButton } from '../../../../features/sign-out';

import classes from './ViewerMenu.module.css';

interface ViewerMenuProps {
  user: Viewer;
}

const DROPDOWN_WIDTH = 250;

export const ViewerMenu: FC<ViewerMenuProps> = ({ user }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [menuPos, setMenuPos] = useState({
    right: '0px',
  });

  const handleAvatarClick: MouseEventHandler<HTMLDivElement> = (e) => {
    const coord = e.currentTarget.getBoundingClientRect();

    setMenuPos({
      right: `-${coord.right - DROPDOWN_WIDTH}px`,
    });

    setDropdownOpen(!dropdownOpen);
  };

  const handleClickOutside: MouseEventHandler<HTMLDivElement> = () => {
    setDropdownOpen(false);
  };

  return (
    <>
      <ViewerAvatar
        user={user}
        className={classes.avatarViewer}
        onClick={handleAvatarClick}
      />
      {dropdownOpen && (
        <div className={classes.dropdownWrapper} onClick={handleClickOutside}>
          <div
            className={classes.dropdown}
            style={menuPos}
            onClick={(e) => e.stopPropagation()}
          >
            <ViewerCard user={user} />
            <SignOutButton className={classes.signOutBtn}>Выйти</SignOutButton>
          </div>
        </div>
      )}
    </>
  );
};
