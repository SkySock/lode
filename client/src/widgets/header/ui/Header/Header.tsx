import { FC, MouseEventHandler, useEffect, useState } from 'react';
import { useViewerStore } from '../../../../entities/viewer';
import { PostCreateForm } from '../../../../features/create-post';
import { AuthLinks } from '../AuthLinks';
import { CreatePostButton } from '../CreatePostButton';
import { HeadLogo } from '../HeadLogo';
import { ViewerMenu } from '../ViewerMenu';

import classes from './Header.module.css';

export const Header: FC = () => {
  const user = useViewerStore((state) => state.user);
  const syncViewer = useViewerStore((state) => state.syncUser);
  const [createPost, setCreatePost] = useState(false);

  useEffect(() => {
    syncViewer();
  }, [syncViewer]);

  const createButtonHandler: MouseEventHandler<HTMLButtonElement> = () => {
    setCreatePost(true);
  };

  const closeCreatePostFormHandler = () => {
    setCreatePost(false);
  };

  return (
    <div className={classes.header}>
      <HeadLogo />
      {!user && <AuthLinks />}
      {!!user && (
        <div className={classes.viewerMenu}>
          <CreatePostButton onClick={createButtonHandler} />
          <ViewerMenu user={user} />
        </div>
      )}
      {createPost && <PostCreateForm onClose={closeCreatePostFormHandler} />}
    </div>
  );
};
