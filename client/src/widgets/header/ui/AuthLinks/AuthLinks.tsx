import { Link } from '@tanstack/react-router';
import { FC } from 'react';
import classes from './AuthLinks.module.css';

export const AuthLinks: FC = () => {
  return (
    <div className={classes.linkWrapper}>
      <Link to="/auth" search={{ type: 'sign-up' }} className={classes.link}>
        SignUp
      </Link>
      <Link to="/auth" search={{ type: 'sign-in' }} className={classes.link}>
        SignIn
      </Link>
    </div>
  );
};
