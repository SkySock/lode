import { FC, MouseEventHandler } from 'react';
import plusIcon from '../../../../shared/assets/plus.svg';
import classes from './CreatePostButton.module.css';

interface CreatePostButtonProps {
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export const CreatePostButton: FC<CreatePostButtonProps> = ({ onClick }) => {
  return (
    <button className={classes.btn} onClick={onClick}>
      <img className={classes.img} src={plusIcon} />
    </button>
  );
};
