import { Link } from '@tanstack/react-router';
import { FC } from 'react';
import classes from './HeadLogo.module.css';

export const HeadLogo: FC = () => {
  return (
    <Link to="/" className={classes.logo}>
      LODE
    </Link>
  );
};
