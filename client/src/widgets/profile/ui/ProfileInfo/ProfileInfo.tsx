import { FC } from 'react';
import { useProfileStore } from '../../../../entities/profile';
import { useViewerStore } from '../../../../entities/viewer';
import { FollowButton } from '../../../../features/follow';
import { Profile } from '../../../../shared/api';
import { AvatarImg, Counter } from '../../../../shared/ui';

import classes from './ProfileInfo.module.css';

interface ProfileInfoProps {
  profileData?: Profile;
}

export const ProfileInfo: FC<ProfileInfoProps> = () => {
  const viewer = useViewerStore((state) => state.user);
  const profile = useProfileStore((state) => state.profile);
  const incFollowers = useProfileStore((state) => state.incFollowers);
  const decFollowers = useProfileStore((state) => state.decFollowers);

  return (
    <div className={classes.profileWrapper}>
      <AvatarImg src={profile.avatarUrl} />
      <div className={classes.nameHeader}>
        <h2 className={classes.displayName}>{profile.displayName}</h2>
        <div className={classes.username}>@{profile.username}</div>
      </div>
      <p>{profile.bio}</p>
      {!!profile && (
        <Counter value={profile.followersCount} label="подписчики" />
      )}
      {!!viewer && viewer?.id !== profile.id && !!profile && (
        <FollowButton
          followOnUsername={profile.username}
          onFollow={incFollowers}
          onRemoveFollow={decFollowers}
        />
      )}
    </div>
  );
};
