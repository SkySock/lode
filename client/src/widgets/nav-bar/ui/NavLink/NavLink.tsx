import { Link, LinkProps } from '@tanstack/react-router';
import { FC, ReactNode } from 'react';
import classes from './NavLink.module.css';

interface NavLinkProps extends LinkProps {
  iconSrc?: string;
  children?: ReactNode;
}

const activeStile = {
  backgroundColor: '#81a1c1',
  fontWeight: 'bold',
};

export const NavLink: FC<NavLinkProps> = ({
  iconSrc,
  children,
  ...linkProps
}) => {
  return (
    <Link
      activeProps={{ style: activeStile }}
      {...linkProps}
      className={classes.link}
    >
      <img src={iconSrc} className={classes.img} />
      {children}
    </Link>
  );
};
