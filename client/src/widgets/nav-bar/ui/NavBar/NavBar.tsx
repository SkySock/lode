import { FC, useEffect, useState } from 'react';
import { useViewerStore } from '../../../../entities/viewer';
import { profilesApi, ProfileTinyInfo } from '../../../../shared/api';
import homeIcon from '../../../../shared/assets/home.svg';
import profileIcon from '../../../../shared/assets/profile.svg';
import { AvatarImg } from '../../../../shared/ui';
import { NavLink } from '../NavLink';
import classes from './NavBar.module.css';

export const NavBar: FC = () => {
  const viewer = useViewerStore((state) => state.user);
  const [follows, setFollows] = useState<ProfileTinyInfo[]>([]);

  useEffect(() => {
    profilesApi.getFollows().then(({ data }) => {
      profilesApi
        .getProfilesInfo(data.map((p) => p.profileId))
        .then(({ data }) => setFollows(data));
    });
  }, [viewer]);

  return (
    <div className={classes.navbar}>
      <NavLink to="/" iconSrc={homeIcon}>
        Домашняя
      </NavLink>
      {!!viewer && (
        <NavLink
          to="/profiles/$username"
          params={{ username: viewer.username }}
          activeOptions={{ exact: true }}
          iconSrc={profileIcon}
        >
          Мой профиль
        </NavLink>
      )}
      {follows.map((profile) => (
        <NavLink
          to="/profiles/$username"
          params={{ username: profile.username }}
          key={'navFollows' + profile.id}
        >
          <AvatarImg
            src={profile.avatarUrl}
            className={classes.profileAvatar}
          />
          @{profile.username}
        </NavLink>
      ))}
    </div>
  );
};
